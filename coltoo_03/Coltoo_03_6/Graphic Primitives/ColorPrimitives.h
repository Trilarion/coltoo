///////////////////////////////////////////////////////////
//  ColorPrimitives.h
//  Implementation of the Class ColorPrimitives
//  Created on:      16-Jul-2004 13:49:53
///////////////////////////////////////////////////////////

#if !defined(COLORPRIMITIVES__INCLUDED_)
#define COLORPRIMITIVES__INCLUDED_

#include <SDL/SDL_types.h>

class ColorPrimitives {
private:
  Uint32 mvarRed;
  Uint32 mvarGreen;
  Uint32 mvarBlue;
  
public:
	ColorPrimitives();
	ColorPrimitives(Uint32 red, Uint32 green, Uint32 blue);
	virtual ~ColorPrimitives();
	Uint32 Red();
	Uint32 Green();
	Uint32 Blue();
	void setRed(Uint32 red);
	void setGreen(Uint32 green);
	void setBlue(Uint32 blue);
};

#endif // !defined(COLORPRIMITIVES__INCLUDED_)
