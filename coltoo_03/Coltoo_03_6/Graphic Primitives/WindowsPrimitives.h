///////////////////////////////////////////////////////////
//  WindowsPrimitives.h
//  Implementation of the Class WindowsPrimitives
//  Created on:      16-Jul-2004 13:49:56
///////////////////////////////////////////////////////////

#if !defined(WINDOWSPRIMITIVE__INCLUDED_)
#define WINDOWSPRIMITIVE__INCLUDED_

#include "SurfacePrimitives.h"
#include "FontPrimitives.h"

class WindowsPrimitives : virtual public SurfacePrimitives
{
private:
  SDL_Surface *screen;
  FontPrimitives *font;

public:
  WindowsPrimitives();
	WindowsPrimitives(const char *name, SDL_Surface * screen, int x, int y, int w, int h, TTF_Font * font);
	WindowsPrimitives(const char *name, SDL_Surface * screen, int x, int y, int w, int h, SDL_Surface * font);
	virtual ~WindowsPrimitives();
	void createWindow(const char *name, SDL_Surface * screen, int x, int y, int w, int h, TTF_Font * font);
	void createWindow(const char *name, SDL_Surface * screen, int x, int y, int w, int h, SDL_Surface * font);
	void DisplayWindow(const char* text);
	void MessageWindow(int message);
	void SysMessageWindow(int sysMessage);

};




#endif // !defined(WINDOWSPRIMITIVE__INCLUDED_)
