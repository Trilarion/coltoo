///////////////////////////////////////////////////////////
//  FontPrimitives.h
//  Implementation of the Class FontPrimitives
//  Created on:      16-Jul-2004 13:49:53
///////////////////////////////////////////////////////////

#if !defined(FONTPRIMITIVES__INCLUDED_)
#define FONTPRIMITIVES__INCLUDED_

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "SurfacePrimitives.h"

struct SDLFont {
  TTF_Font	*font;
  SDL_Color	color;
};

class FontPrimitives : public SurfacePrimitives {

public:
	FontPrimitives();
	virtual ~FontPrimitives();
	bool createFont(SDLFont font);
	bool createFont(SDL_Surface * surface);
	void drawString(SDL_Surface* screen, int x, int y, const char* format);
	void drawShadowString(SDL_Surface* screen, int x, int y, bool shadow, const char* format);
	void blitString(SDL_Surface* screen, int x, int y, const char* text);
	void blitString(SDL_Surface* screen, int x, int y, const char* text, SDL_Color color);
	void drawColorString(SDL_Surface * screen, ColorPrimitives * color1, ColorPrimitives * color2);
	void setFontColor(ColorPrimitives * color1, ColorPrimitives * color2);
	void initTrueType();
	void quitTrueType();

};


#endif // !defined(FONTPRIMITIVES__INCLUDED_)
