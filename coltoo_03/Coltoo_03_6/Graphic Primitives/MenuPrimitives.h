///////////////////////////////////////////////////////////
//  MenuPrimitives.h
//  Implementation of the Class MenuPrimitives
//  Created on:      28-Jul-2004 18:31:29
///////////////////////////////////////////////////////////

#if !defined(EA_D2C427C9_732F_4125_811C_647FEE31C890__INCLUDED_)
#define EA_D2C427C9_732F_4125_811C_647FEE31C890__INCLUDED_

#include "WidgetPrimitives.h"

class MenuPrimitives : public WidgetPrimitives
{

public:
    MenuPrimitives();
	virtual ~MenuPrimitives();
	bool addMenu(const char * label);
	void delMenu(int index);
	void delMenu(const char * label);
	int MenuSelected();

};




#endif // !defined(EA_D2C427C9_732F_4125_811C_647FEE31C890__INCLUDED_)
