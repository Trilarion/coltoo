///////////////////////////////////////////////////////////
//  MapSurfacePrimitives.cpp
//  Implementation of the Class MapSurfacePrimitives
//  Created on:      16-Jul-2004 13:49:55
///////////////////////////////////////////////////////////

#include "MapSurfacePrimitives.h"

MapSurfacePrimitives::MapSurfacePrimitives(){

}


MapSurfacePrimitives::~MapSurfacePrimitives(){

}


bool MapSurfacePrimitives::createMap(Map * map){

}


void MapSurfacePrimitives::drawTerrain(){

}


void MapSurfacePrimitives::drawOverlays(){

}


void MapSurfacePrimitives::drawShroud(Actor * player){

}


void MapSurfacePrimitives::drawUnits(){

}


void MapSurfacePrimitives::drawRoad(int screenTile, long mapTile, int icon){

}


void MapSurfacePrimitives::drawLinks(int screenTile, long mapTile, TileTypes tileType){

}



