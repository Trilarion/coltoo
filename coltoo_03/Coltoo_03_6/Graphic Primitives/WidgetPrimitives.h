///////////////////////////////////////////////////////////
//  WidgetPrimitives.h
//  Implementation of the Class WidgetPrimitives
//  Created on:      28-Jul-2004 18:01:30
///////////////////////////////////////////////////////////

#if !defined(WIDGETPRIMITIVES__INCLUDED_)
#define WIDGETPRIMITIVES__INCLUDED_

#include <SDL/SDL.h>
#include "SurfacePrimitives.h"
#include "FontPrimitives.h"

class WidgetPrimitives : public SurfacePrimitives
{

public:
  WidgetPrimitives();
	virtual ~WidgetPrimitives();

public:
	void draw();
	FontPrimitives * getFont();
	void setFont(FontPrimitives * newVal);
	bool IsVisible();
	void setVisibility(bool visible);
	bool IsMouseOver(int x, int y, int w, int h);
	void create(SDL_Surface * screen, int x, int y, int w, int h);

private:
	FontPrimitives * font;

};

#endif // !defined(WIDGETPRIMITIVES__INCLUDED_)
