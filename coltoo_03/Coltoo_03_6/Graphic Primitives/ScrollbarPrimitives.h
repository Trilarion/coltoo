///////////////////////////////////////////////////////////
//  ScrollbarPrimitives.h
//  Implementation of the Class ScrollbarPrimitives
//  Created on:      28-Jul-2004 18:16:51
///////////////////////////////////////////////////////////

#if !defined(SCROLLBARPRIMITIVES__INCLUDED_)
#define SCROLLBARPRIMITIVES__INCLUDED_

#include <SDL/SDL.h>
#include "WidgetPrimitives.h"
#include "ButtonPrimitives.h"

class ScrollbarPrimitives : public WidgetPrimitives {

public:
  ScrollbarPrimitives();
	virtual ~ScrollbarPrimitives();

public:
	void create(SDL_Surface * screen, int x, int y, int w, int h);
	bool IsScrolling();

private:
	ButtonPrimitives * bttn1;
	ButtonPrimitives * bttn2;
	ButtonPrimitives * bttn3;

};




#endif // !defined(SCROLLBARPRIMITIVES__INCLUDED_)
