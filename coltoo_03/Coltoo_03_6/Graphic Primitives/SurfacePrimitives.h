///////////////////////////////////////////////////////////
//  SurfacePrimitives.h
//  Implementation of the Class SurfacePrimitives
//  Created on:      16-Jul-2004 13:49:56
///////////////////////////////////////////////////////////

#if !defined(SURFACEPRIMITIVES__INCLUDED_)
#define SURFACEPRIMITIVES__INCLUDED_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "GraphicPrimitives.h"

class SurfacePrimitives : public GraphicPrimitives {
private:
  SDL_Surface *screen;
  SDL_Surface *mask;
  
public:
	SurfacePrimitives();
	virtual ~SurfacePrimitives();
	void SetSurface(SDL_Surface * surface);
	SDL_Surface *GetSurface();
	void SetMask(SDL_Surface *nmask);
	SDL_Surface *GetMask();
	void createSurface(SDL_Surface *screen, int w, int h);
	void fillSurface(int x, int y, int w, int h, ColorPrimitives * color);
	bool loadImage(const char* filename);
	void drawTILE(SDL_Surface * from, int x, int y, int w, int h, int x2, int y2, ColorPrimitives * color);
	void drawSURF(SDL_Surface * from, int x, int y, ColorPrimitives * color);
	Uint32 getPixel(int x, int y);
	Uint32 getPixel(SDL_Surface *from, int x, int y);
	void putPixel(int x, int y, Uint32 pixel);
	bool createMask(SDL_Surface * surface);

};




#endif // !defined(SURFACEPRIMITIVES__INCLUDED_)
