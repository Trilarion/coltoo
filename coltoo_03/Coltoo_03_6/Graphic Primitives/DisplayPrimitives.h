///////////////////////////////////////////////////////////
//  DisplayPrimitives.h
//  Implementation of the Class DisplayPrimitives
//  Created on:      16-Jul-2004 13:49:53
///////////////////////////////////////////////////////////

#if !defined(DISPLAYPRIMITIVES__INCLUDED_)
#define DISPLAYPRIMITIVES__INCLUDED_

#include "ScreenPrimitives.h"
#include "MapSurfacePrimitives.h"
#include "WindowsPrimitives.h"
#include "../Gamecore/Map.h"

class DisplayPrimitives : public ScreenPrimitives {

public:
	DisplayPrimitives(Map * map, int height, int width);
	virtual ~DisplayPrimitives();
	int getDisplayWidth();
	int getDisplayHeight();

private:
	MapSurfacePrimitives *mapsp;
  WindowsPrimitives *displayWindow;
  WindowsPrimitives *miniMapDisplay;
};

#endif // !defined(DISPLAYPRIMITIVES__INCLUDED_)
