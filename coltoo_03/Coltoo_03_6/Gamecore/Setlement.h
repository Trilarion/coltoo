///////////////////////////////////////////////////////////
//  Setlement.h
//  Implementation of the Class Settlement
//  Created on:      15-Jul-2004 0:07:30
///////////////////////////////////////////////////////////

#if !defined(SETLEMENT__INCLUDED_)
#define SETLEMENT__INCLUDED_

#include <vector>
#include "Buildings.h"
#include "Market.h"

using namespace std;

class Setlement {

public:
	Setlement();
	virtual ~Setlement();

private:
  vector<Buildings> *buildings;
  Market *market;
};


#endif // !defined(SETLEMENT__INCLUDED_)

