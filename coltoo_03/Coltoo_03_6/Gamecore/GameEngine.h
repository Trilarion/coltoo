///////////////////////////////////////////////////////////
//  GameEngine.h
//  Implementation of the Class GameEngine
//  Created on:      15-Jul-2004 0:30:02
///////////////////////////////////////////////////////////

#if !defined(GameEngine__INCLUDED_)
#define GameEngine__INCLUDED_

#include <vector>
#include "GraphicEngine.h"
#include "Command.h"
#include "CombatEngine.h"

using namespace std;

class GameEngine {
private:
  GraphicEngine *graph;
  vector<Command*> commands;
  CombatEngine *combat;
  bool localGE;

public:
	GameEngine();
	GameEngine(GraphicEngine *graphics);
	virtual ~GameEngine();
	bool Init(int argc, char **argv);
	void GameLoop();

};




#endif // !defined(GameEngine__INCLUDED_)
