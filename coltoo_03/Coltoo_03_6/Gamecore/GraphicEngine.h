///////////////////////////////////////////////////////////
//  GraphicEngine.h
//  Implementation of the Class GraphicEngine
//  Created on:      15-Jul-2004 1:11:05
///////////////////////////////////////////////////////////

#if !defined(GRAPHICENGINE__INCLUDED_)
#define GRAPHICENGINE__INCLUDED_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_rotozoom.h>
#include <SDL/SDL_ttf.h>

using namespace std;

#include "Map.h"
#include "Unit.h"

class GraphicEngine {
private:
  bool initiated;
  
public:
	GraphicEngine();
	virtual ~GraphicEngine();
	bool createDisplay(Map * map, int height, int width);
	void DrawScene();
	void BlinkUnit(Unit * unit, long update, bool blink);
	bool IsOnScreen(int x, int y);
	bool IsOnScreen(long startMapTile, long mapTile);
	bool createMap(Map * map);
	void drawMap(int center_x, int center_y);
	void drawBackground();
	void updateUnitInfo(Unit * unit);
	long getScreenTileAt(int x, int y);
	int getXPosition(long tile);
	void getYPosition(long tile);

};




#endif // !defined(GRAPHICENGINE__INCLUDED_)
