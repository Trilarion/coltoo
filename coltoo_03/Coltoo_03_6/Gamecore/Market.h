///////////////////////////////////////////////////////////
//  Market.h
//  Implementation of the Class Market
//  Created on:      15-Jul-2004 0:18:32
///////////////////////////////////////////////////////////

#if !defined(MARKET__INCLUDED_)
#define MARKET__INCLUDED_

#include "Buildings.h"

class Market : public Buildings
{

public:
	Market();
	virtual ~Market();

};

#endif // !defined(MARKET__INCLUDED_)
