///////////////////////////////////////////////////////////
//  Universe.h
//  Implementation of the Class Universe
//  Created on:      15-Jul-2004 0:01:23
///////////////////////////////////////////////////////////

#if !defined(UNIVERSE__INCLUDED_)
#define UNIVERSE__INCLUDED_

#include "Map.h"

class Universe {

public:
	Universe();
	virtual ~Universe();

private:
  Map *map;
};


#endif // !defined(UNIVERSE__INCLUDED_)
