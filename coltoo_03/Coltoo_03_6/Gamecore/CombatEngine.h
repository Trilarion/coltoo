///////////////////////////////////////////////////////////
//  CombatEngine.h
//  Implementation of the Class CombatEngine
//  Created on:      15-Jul-2004 0:32:39
///////////////////////////////////////////////////////////

#if !defined(COMBATENGINE__INCLUDED_)
#define COMBATENGINE__INCLUDED_

class CombatEngine {

public:
	CombatEngine();
	virtual ~CombatEngine();

};

#endif // !defined(COMBATENGINE__INCLUDED_)
