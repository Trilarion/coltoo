///////////////////////////////////////////////////////////
//  Unit.h
//  Implementation of the Class Unit
//  Created on:      15-Jul-2004 0:20:18
///////////////////////////////////////////////////////////

#if !defined(UNIT__INCLUDED_)
#define UNIT__INCLUDED_

class Unit
{

public:
	Unit();
	virtual ~Unit();

};

#endif // !defined(UNIT__INCLUDED_)
