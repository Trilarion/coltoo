///////////////////////////////////////////////////////////
//  SoundEngine.h
//  Implementation of the Class SoundEngine
//  Created on:      15-Jul-2004 1:11:13
///////////////////////////////////////////////////////////

#if !defined(SOUNDENGINE__INCLUDED_)
#define SOUNDENGINE__INCLUDED_

class SoundEngine {

public:
	SoundEngine();
	virtual ~SoundEngine();

};

#endif // !defined(SOUNDENGINE__INCLUDED_)
