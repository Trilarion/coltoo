///////////////////////////////////////////////////////////
//  GraphicEngine.cpp
//  Implementation of the Class GraphicEngine
//  Created on:      15-Jul-2004 1:11:05
///////////////////////////////////////////////////////////

#include <iostream>
#include "GraphicEngine.h"

GraphicEngine::GraphicEngine() {
  initiated = false;
  if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
    cout << "Unable to init SDL; " << SDL_GetError() << endl;
    initiated = true;
  }
}



GraphicEngine::~GraphicEngine() {
  SDL_Quit();
}


bool GraphicEngine::createDisplay(Map * map, int height, int width){

}


void GraphicEngine::DrawScene(){

}


void GraphicEngine::BlinkUnit(Unit * unit, long update, bool blink){

}


bool GraphicEngine::IsOnScreen(int x, int y){

}


bool GraphicEngine::IsOnScreen(long startMapTile, long mapTile){

}
