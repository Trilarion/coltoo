///////////////////////////////////////////////////////////
//  OutputDispatcher.h
//  Implementation of the Class OutputDispatcher
//  Created on:      15-Jul-2004 0:44:55
///////////////////////////////////////////////////////////

#if !defined(OUTPUTDISPATCHER__INCLUDED_)
#define OUTPUTDISPATCHER__INCLUDED_

class OutputDispatcher {

public:
	OutputDispatcher();
	virtual ~OutputDispatcher();

};


#endif // !defined(OUTPUTDISPATCHER__INCLUDED_)
