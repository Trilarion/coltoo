///////////////////////////////////////////////////////////
//  Tile.h
//  Implementation of the Class Tile
//  Created on:      15-Jul-2004 0:01:42
///////////////////////////////////////////////////////////

#if !defined(TILE__INCLUDED_)
#define TILE__INCLUDED_

#include "Setlement.h"

enum TileTypes { TILE_ROAD=1, TILE_RAIN , TILE_FOREST  , TILE_FAKEFOREST  ,
                 TILE_WATER , TILE_CLEAR, TILE_MOUNTAIN, TILE_FAKEMOUNTAIN,
                 TILE_PLOWED, TILE_RIVER, TILE_HILL    , TILE_FAKEHILL    ,
                 TILE_BUILD , TILE_UNIT , TILE_PEAK    , TILE_FAKEPEAK    ,
                 TILE_FAKE  , TILE_RUMOR, TILE_FAKERUMOR };

class Tile {

public:
	Tile();
	virtual ~Tile();

private:
  Setlement *setlement;
};




#endif // !defined(TILE__INCLUDED_)
