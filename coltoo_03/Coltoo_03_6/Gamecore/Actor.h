///////////////////////////////////////////////////////////
//  Actor.h
//  Implementation of the Class Actor
//  Created on:      15-Jul-2004 0:20:25
///////////////////////////////////////////////////////////

#if !defined(ACTOR__INCLUDED_)
#define ACTOR__INCLUDED_

#include <vector>
#include "Setlement.h"
#include "Unit.h"

using namespace std;

class Actor
{

public:
	Actor();
	virtual ~Actor();

private:
  vector<Setlement*> setlements;
  vector<Unit*> units;
};




#endif // !defined(ACTOR__INCLUDED_)
