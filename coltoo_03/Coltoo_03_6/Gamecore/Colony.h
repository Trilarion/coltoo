///////////////////////////////////////////////////////////
//  Colony.h
//  Implementation of the Class Colony
//  Created on:      15-Jul-2004 0:12:17
///////////////////////////////////////////////////////////

#if !defined(COLONY__INCLUDED_)
#define COLONY__INCLUDED_

#include "Setlement.h"

class Colony : public Setlement {

public:
	Colony();
	virtual ~Colony();

};


#endif // !defined(COLONY__INCLUDED_)
