///////////////////////////////////////////////////////////
//  Village.h
//  Implementation of the Class Village
//  Created on:      15-Jul-2004 0:12:10
///////////////////////////////////////////////////////////

#if !defined(VILLAGE__INCLUDED_)
#define VILLAGE__INCLUDED_

#include "Setlement.h"

class Village : public Setlement {

public:
	Village();
	virtual ~Village();

};


#endif // !defined(VILLAGE__INCLUDED_)
