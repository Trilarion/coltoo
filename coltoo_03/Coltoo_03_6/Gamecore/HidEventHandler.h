///////////////////////////////////////////////////////////
//  HidEventHandler.h
//  Implementation of the Class HidEventHandler
//  Created on:      15-Jul-2004 0:28:09
///////////////////////////////////////////////////////////

#if !defined(HIDEVENTHANDLER__INCLUDED_)
#define HIDEVENTHANDLER__INCLUDED_

#include "InputHandler.h"

class HidEventHandler : public InputHandler {

public:
	HidEventHandler();
	virtual ~HidEventHandler();

};




#endif // !defined(HIDEVENTHANDLER__INCLUDED_)
