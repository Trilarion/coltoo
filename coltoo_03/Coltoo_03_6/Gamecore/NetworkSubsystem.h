///////////////////////////////////////////////////////////
//  NetworkSubsystem.h
//  Implementation of the Class NetworkSubsystem
//  Created on:      15-Jul-2004 0:49:03
///////////////////////////////////////////////////////////

#if !defined(NETWORKSUBSYSTEM__INCLUDED_)
#define NETWORKSUBSYSTEM__INCLUDED_

#include "InputHandler.h"

class NetworkSubsystem : public InputHandler {

public:
	NetworkSubsystem();
	virtual ~NetworkSubsystem();

};

#endif // !defined(NETWORKSUBSYSTEM__INCLUDED_)
