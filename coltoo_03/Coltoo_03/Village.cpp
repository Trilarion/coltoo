/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
// FILE		: Village.cpp
//
// VERSION	: 0.1
//
// AUTHOR	: Chainsword,  June 2004
//
// CLASS	: Village
//
// DESCRIPTION	: Basis class for village control.
//

#include "Village.h"
#include "gameEngine.h"
#include "map.h"

Village::Village()
{
    m_pGame = NULL;
    villageID = 0;
    villageName = "Village";
    villagePlural = "Villages";
}

Village::Village(GameEngine *g) : m_pGame(g)
{
    villageID = 0;
    villageName = "Village";
    villagePlural = "Villages";
}

Village::~Village()
{
    deleteUnits();
}

void Village::InitUnits(Uint8 nativeID, int techLevel)
{
    if(!m_pGame) return;
    Map *map = m_pGame->GetMap();
    int i;
    maxUnits = (rand() % 10 + 1) * (techLevel+1) + (rand() % 10);
    #ifdef DEBUG
    cout<<"        Placing "<<maxUnits<<" units"<<endl;
    #endif
    Unit *u1 = Unit::loadUnitFile(IDU_BRAVES);
    Unit *u2 = Unit::loadUnitFile(IDU_MTDBRAVES);
    for(i=0; i<maxUnits; i++)
    {
        Unit *u;
        if((techLevel>=2) && ((rand()%10)==1)) u = new Unit(u2);
        else u = new Unit(u1);
        if(!u) break;
        units.push_back(u);
        u->setMovesLeft(u->getMoveCapability());
        u->setUnitID(units.size());
        u->setNation(nativeID);
        if(map)
        {
            Tile *t = map->getTile(villageTile);
            if(t)
            {
                u->setPositionX(t->getXPos());
                u->setPositionY(t->getYPos());
                u->setTile(villageTile);
                u->setStartNumber(villageTile - 6 * map->GetWidth()- 5);
                t->setWalkingUnit(u);
                //t->setFlags(TILE_UNIT, true);
            }
        }
    }
}

void Village::deleteUnits()
{
    Map *map = m_pGame->GetMap();
    Tile *t;
    vector<Unit*>::iterator it = units.begin();
    for( it = units.begin(); it != units.end(); it++ )
    {
        if (*it)
        {
            Unit *u = (Unit *) *it;
            if(u)
            {
                t = map->getTile(u->getTile());
                if(t)
                {
                    t->removeWalkingUnit(u);
                    t->setFlags(TILE_UNIT, false);
                }
                delete (*it);
                u = NULL;
            }
        }
    }
    units.clear();
}

//GET Methods
int Village::ID()
{
    return villageID;
}

char *Village::Name()
{
    return (char *)villageName.c_str();
}

char *Village::Plural()
{
    return (char *) villagePlural.c_str();
}

long Village::getTile()
{
    return villageTile;
}
Unit *Village::getUnit(int u)
{
    if((u>=0) && (u<units.size())) return units[u];
    return NULL;
}
int Village::getNumUnits()
{
    return units.size();
}

//SET Methods
void Village::ID(int id)
{
    villageID = id;
}

void Village::Name(const char *n)
{
    villageName = n;
}

void Village::Plural(const char *p)
{
    villagePlural = p;
}

void Village::setTile(long t)
{
    villageTile = t;
}

//ADD Methods
void Village::addUnit(Unit *u)
{
    if(units.size()<maxUnits) units.push_back(u);
}

//DESTROY Methods
void Village::destroyUnit(int uID)
{
    if((uID>0) && (uID<units.size()))
    {
        if(units[uID])
        {
            Tile *t = m_pGame->GetMap()->getTile(units[uID]->getTile());
            if(t)
            {
                t->removeWalkingUnit(units[uID]);
                t->setFlags(TILE_UNIT, t->hasUnitsWalking());
            }
            delete(units[uID]);
            units[uID] = units[units.size()-1];
            units.pop_back();
        }
    }
}

