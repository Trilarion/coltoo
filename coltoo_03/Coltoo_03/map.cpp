/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: map.cpp
//
// VERSION	: 0.1
//
// AUTHOR	: Brett Anthoine,  November 2003
//
// CLASS	: Map
//
// DESCRIPTION	: Map of tiles
//

#include "map.h"
#include <SDL/SDL_types.h>
#include "GraphicsEngine.h"

//Constructors, destructor
Map::Map(GameEngine *g) : m_pGame(g)
{
  long i=0;
  width  = 0;
  height = 0;
  tileTypes = NULL;
}

Map::Map(GameEngine *g,int w,int h) : m_pGame(g),width(w),height(h)
{
  long i=0;
  Tile *t = NULL;
  tileTypes = NULL;
  #ifdef DEBUG
  cout<<"  Creating map["<<width<<","<<height<<"]"<<endl;
  #endif
  for(i=0;i<width * height;i++)
    tiles.push_back(t);
}

Map::~Map()
{
  int i = 0;

  #ifdef DEBUG
  cout << "deleting map tiles" << endl;
  #endif
  if(tileTypes) delete tileTypes;

  for(i = 0; i < tiles.size(); i++)
    if(tiles[i]) delete tiles[i];
}

//Methods
bool Map::Init()
{
  GlobalData* data = m_pGame->GetData();

  int x=0,y=0,k=0,x1,y1,land=0,x2;
  long ii=0, i=0;
  bool rumor;
    
  #ifdef DEBUG
  cout<<"  Initializing Map"<<endl;
  #endif
  //added 20/6
  tileTypes = new CTileTypes();
  tileTypes->InitTileTypes();

  //added 04/07
  //if width*height==0 do nothing
  if(width*height==0) return false;
    
  //if tiles.size()==0 init tiles
  if(tiles.size()==0)
  {
    for(ii=0; ii<width*height; ii++)
    {
      Tile *t = new Tile();
      tiles.push_back(t);
    }
  }
        
  //added 5/25
  //places a row of arctic tiles along top and bottom edges of map
  for(ii=0;ii<width;ii++)
  {
    setArctic(ii);
    setArctic(ii+(width*(height-1)));
  }

  //added 5/25
  //adds a column of Ocean and HighSeas to the sides of the map
  for(ii=width;ii<width*(height-1);ii+=width)
  {
    setHighseas(ii);
    setHighseas(ii+1);
    setHighseas(ii+(width-1));
    setHighseas(ii+(width-2));
  }

  //added 5/25
  //checks the east third of map to see if tile is within 3 tiles of land
  for(y=4*width; y<width*(height-4); y+=width)
  {
    for(x=width-2; x>99; x--)
    {
      if(tiles[y+x]->getTerrain()==TERRAIN_OCEAN)
      {
        land = 0; //changed 04/07; special vars should be initialized in the begining of the loop
        for(y1=y-3*width; y1<y+4*width; y1+=width)
        {
          //if tile is too close to east edge put a limit on tiles checked
          if(x==width-2) x2=0;
          else if(x==width-3) x2=1;
          else if(x==width-4) x2=2;
          else if(x==width-5) x2=3;
          else x2=4;

          for(x1=x-3;x1<x+x2;x1++)
          {
            if(!(tiles[y1+x1]->getTerrain()==TERRAIN_OCEAN || tiles[y1+x1]->getTerrain()==TERRAIN_HIGHSEAS))
            {
              land=1;
              break; //added 04/07; no need to continue the loop once a land was found
            }
          }
        }
        if(land==0 && tiles[y+x+1]->getTerrain()==TERRAIN_HIGHSEAS)
        {
          //tiles[y+x]->setTerrain(TERRAIN_HIGHSEAS);
          setHighseas(y+x); //added 04/07; reset flag related with TERRAIN_HIGHSEAS
        }
      }
    }
  }    

  //Initialize each tile properties and add random flags
  for(y=0, ii=0; y < height; y++)
  {
    for(x=0; x < width; x++, ii++)
    {
      tiles[ii]->setXPos(x);
      tiles[ii]->setYPos(y);
      tiles[ii]->setShrdstate(2);
      tiles[ii]->setFkterrain(0);
      tiles[ii]->setVillages(0);

      //  If not water or river, randomly make it a fake tile
      //changed 04/07; if water or river no need to check the rand()
      if(!tiles[ii]->Is(TILE_WATER) && !tiles[ii]->Is(TILE_RIVER) &&
         (ii>width-1) && (ii<width*(height-1)-1))
      {
        k=(rand()%8);
        if(k==1)
        {
          FalseTerrain(tiles[ii]);
        }
      }

      // If not water or a Peak (unreachable terrain for a
      // ground unit) randomly put a rumor on it
      if(!tiles[ii]->Is(TILE_WATER) && !tiles[ii]->Is(TILE_PEAK) &&
          ii>width && ii<data->mapmax-width)
      {
        k=(rand()%40);

        //changed 5/25
        //prevents rumors from being next to each other (real ones)
        if(k==1)
        {
          rumor = false;
          for(i=ii-width-1; i<ii-width+2; i++)
          {
            if(tiles[i]->Is(TILE_RUMOR))
            {
              rumor = true;
              break;
            }
          }
          if(!rumor && !tiles[ii-1]->Is(TILE_RUMOR)) tiles[ii]->setFlags(TILE_RUMOR, true);
        }
      }
    }
  }

  //update Tile links for every tile if needed.
  for(y=0, ii=0; y < height; y++)
  {
    for(x=0; x < width; x++, ii++)
    {
      if(tiles[ii]->Is(TILE_PEAK) || tiles[ii]->Is(TILE_FAKEPEAK))
        UpdateTileLinks(TILE_PEAK,x,y);
      if(tiles[ii]->Is(TILE_MOUNTAIN) || tiles[ii]->Is(TILE_FAKEMOUNTAIN))
        UpdateTileLinks(TILE_MOUNTAIN,x,y);
      if(tiles[ii]->Is(TILE_HILL) || tiles[ii]->Is(TILE_FAKEHILL))
        UpdateTileLinks(TILE_HILL,x,y);
      if(tiles[ii]->Is(TILE_FOREST) || tiles[ii]->Is(TILE_FAKEFOREST))
        UpdateTileLinks(TILE_FOREST,x,y);

      if(tiles[ii]->Is(TILE_ROAD))
        UpdateTileLinks(TILE_ROAD,x,y);
      if(tiles[ii]->Is(TILE_RIVER) || tiles[ii]->Is(TILE_WATER)   )
        UpdateTileLinks(TILE_RIVER,x,y);
    }
  }
  return true;
}

void Map::setArctic(int t)
{
  if((t>=0) && (t<tiles.size()))
  {
    tiles[t]->setTerrain(TERRAIN_ARCTIC);
    tiles[t]->setFlags(TILE_WATER, false);

    tiles[t]->setFlags(TILE_RIVER, false);
    tiles[t]->setFlags(TILE_FOREST, false);
    tiles[t]->setFlags(TILE_RAIN, false);
    tiles[t]->setFlags(TILE_HILL, false);
    tiles[t]->setFlags(TILE_MOUNTAIN, false);
    tiles[t]->setFlags(TILE_PEAK, false);
  }
}

void Map::setHighseas(int t)
{
  if((t>=0) && (t<tiles.size()))
  {
    tiles[t]->setTerrain(TERRAIN_HIGHSEAS);
    tiles[t]->setFlags(TILE_WATER, true);

    tiles[t]->setFlags(TILE_RIVER, false);
    tiles[t]->setFlags(TILE_FOREST, false);
    tiles[t]->setFlags(TILE_RAIN, false);
    tiles[t]->setFlags(TILE_HILL, false);
    tiles[t]->setFlags(TILE_MOUNTAIN, false);
    tiles[t]->setFlags(TILE_PEAK, false);
  }
}

void Map::setTileAt(int x,int y,Tile* newTile)
{
  if(x>=0 && x<width && y>=0 && y<height)
  {
    if(y*width+x>=tiles.size())
    {
      int i;
      for(i=tiles.size(); i<y*width+x; i++)
      {
        tiles.push_back((Tile *) NULL);
      }
    }
    tiles[y*width + x] = newTile;
    newTile->setIndex(y*width + x);
    return;
  }
  cout << "setTileAt ERROR: index out of bounds.\n";
}

void Map::setTile(int index,Tile* newTile)
{
  if(index >= 0 && index < height*width)
  {
    if(index>=tiles.size())
    {
      int i;
      for(i=index; i<height*width; i++)
      {
        tiles.push_back((Tile *) NULL);
      }
    }
    tiles[index] = newTile;
    newTile->setIndex(index);
    return;
  }
  cout << "setTile ERROR: index out of bounds. \n";
}
       
Tile*   Map::getTileAt(int x,int y) const
{
  if(x>=0 && x<width && y>=0 && y<height)
    return tiles[y*width + x];
  cout << "getTileAt ERROR: index out of bounds.\n";
  return NULL;
}

Tile*   Map::getTile(int index) const
{
  if(index>=0 && index<height*width)
    return tiles[index];
  cout << "getTile ERROR: index out of bounds. \n";
  return NULL;
}
    
bool Map::loadMap(string fileName) //should read map size from map file
{
  FILE *fp;
  long i;
  char tileChar;

  char mapFileNames[11][10]={"map00.txt","map01.txt","map02.txt","map03.txt",
                             "map04.txt","map05.txt","map06.txt","map07.txt",
                             "map08.txt","map09.txt","map10.txt"};

  char filename[25];
  if(!m_pGame) return false;
  
  GlobalData *data = m_pGame->GetData();

  if(fileName == "")
  {
    int k=rand()%11;
    sprintf(filename,"data/text/%s",mapFileNames[k]);
    #ifdef DEBUG
    cout << "  Loading map " << mapFileNames[k] << endl;
    #endif
  }
  else
  {
    sprintf(filename,"data/text/%s",fileName.c_str());
    #ifdef DEBUG
    cout << "  Loading map " << fileName << endl;
    #endif
  }

  if((fp=fopen(filename, "r")) == NULL)
  {
    cout << "ERROR opening file" << filename << endl << endl;
    return false;
  }
  
  i=0;
  while(((tileChar=fgetc(fp))!=EOF) && (i<width*height))
  {
    Tile *t = createTileFrom(tileChar);
    if(!t) return false;
    t->InitShrdstate(data->playerlist);
    setTile(i,t);
    i++;
  }
  
  fclose(fp);
  return true;
}

Tile *Map::createTileFrom(char tileChar) const
{
  Tile *tile = new Tile();
  switch(tileChar)
  {
    case 'A': tile->setFlags(TILE_RIVER, /*true*/ false);
    case 'a': tile->setTerrain(TERRAIN_OCEAN);
              tile->setFlags(TILE_WATER, true);
              break;

    case 'b': tile->setTerrain(TERRAIN_LAKE);
              tile->setFlags(TILE_WATER, true);
              break;

    case 'C': tile->setFlags(TILE_RIVER, true);
    case 'c': tile->setTerrain(TERRAIN_ARCTIC);
              break;

    case 'D': tile->setFlags(TILE_RIVER, true);
    case 'd': tile->setTerrain(TERRAIN_TUNDRA);
              break;

    case 'E': tile->setFlags(TILE_RIVER, true);
    case 'e': tile->setTerrain(TERRAIN_PRAIRIE);
              break;

    case 'F': tile->setFlags(TILE_RIVER, true);
    case 'f': tile->setTerrain(TERRAIN_GRASS);
              break;

    case 'G': tile->setFlags(TILE_RIVER,true);
    case 'g': tile->setTerrain(TERRAIN_SAVANNAH);
              break;

    case 'H': tile->setFlags(TILE_RIVER, true);
    case 'h': tile->setTerrain(TERRAIN_DESERT);
              break;

    case 'I': tile->setFlags(TILE_RIVER, true);
    case 'i': tile->setTerrain(TERRAIN_TUNDRA); /*boreal*/
              tile->setFlags(TILE_FOREST, true);
              break;

    case 'J': tile->setFlags(TILE_RIVER, true);
    case 'j': tile->setTerrain(TERRAIN_GRASS);  /*conifer*/
              tile->setFlags(TILE_FOREST, true);
              break;

    case 'K': tile->setFlags(TILE_RIVER, true);
    case 'k': tile->setTerrain(TERRAIN_OPENS); /*mixed*/
              tile->setFlags(TILE_FOREST, true);
              break;

    case 'L': tile->setFlags(TILE_RIVER, true);
    case 'l': tile->setTerrain(TERRAIN_PRAIRIE); /*broad*/
              tile->setFlags(TILE_FOREST, true);
              break;

    case 'M': tile->setFlags(TILE_RIVER, true);
    case 'm': tile->setTerrain(TERRAIN_SAVANNAH); /*tropic*/
              tile->setFlags(TILE_FOREST, true);
              break;

    case 'N': tile->setFlags(TILE_RIVER, true);
    case 'n': tile->setTerrain(TERRAIN_SAVANNAH); /*rain*/
              tile->setFlags(TILE_FOREST, true);
              tile->setFlags(TILE_RAIN, true);
              break;

    case 'O': tile->setFlags(TILE_RIVER, true);
    case 'o': tile->setTerrain(TERRAIN_DESERT);  /*shrub*/
              tile->setFlags(TILE_FOREST, true);
              break;

    case 'Q': tile->setFlags(TILE_RIVER, true);
    case 'q': tile->setTerrain(TERRAIN_STEPPE);
              break;

    case 'R': tile->setFlags(TILE_RIVER, true);
    case 'r': tile->setTerrain(TERRAIN_CHAPARRAL);
              break;

    case 'S': tile->setFlags(TILE_RIVER, true);
    case 's': tile->setTerrain(TERRAIN_OPENS);
              break;

    case 'T': tile->setFlags(TILE_RIVER, true);
    case 't': tile->setTerrain(TERRAIN_OPENS);   /*hill*/
              tile->setFlags(TILE_HILL, true);
              break;

    case 'U': tile->setFlags(TILE_RIVER, true);
    case 'u': tile->setTerrain(TERRAIN_TUNDRA);  /*mountain*/
              tile->setFlags(TILE_MOUNTAIN, true);
              break;

    case 'V': tile->setFlags(TILE_RIVER, true);
    case 'v': tile->setTerrain(TERRAIN_ARCTIC);  /*mountain peak*/
              tile->setFlags(TILE_PEAK, true);
              break;

    default:  //tile->setTerrain(TERRAIN_OPENS+37);
              break;
  }
  return tile;
}

void Map::RevealAll()
{
	for(int i=0 ; i < height*width ; i++)
  {
		tiles[i]->setShrdstate(SHROUD_NONE);
	}
}

//added 08/07; Reveals all map for Nation n
void Map::RevealAll(Uint8 n)
{
  for(int i=0; i<height*width; i++)
  {
    tiles[i]->setShrdstate(n, SHROUD_NONE);
  }
}

void Map::Reveal(int pos, Uint8 n)
{
  Reveal(pos, n, 2);
}

void Map::Reveal(int pos, Uint8 n, int radius)
{
  int x, y;
  if((pos>=0) && (pos<tiles.size()))
  {
    for(y=-radius; y<=radius; y++)
    {
      for(x=-radius; x<=radius; x++)
      {
        Tile *t = getTile(pos + (y*width+x));
        if(t)
        {
          if(y<-radius/2 || y>radius/2 || x<-radius/2 || x>radius/2)
          {
            if(t->getShrdstate(n)==SHROUD_FULL) t->setShrdstate(n,SHROUD_PART);
          }
          else t->setShrdstate(n, SHROUD_NONE);
        }
      }
    }
  }
}

CTileTypes *Map::TileTypes(void)
{
  return tileTypes;
}

CTileType *Map::TileType(Tile *t)
{
  #ifdef DEBUG
  if(t->Is(TILE_FOREST)) cout<<"Getting forested terrain "<<t->getTerrain()<<endl;
  else cout<<"Getting terrain "<<t->getTerrain()<<endl;
  #endif
  return tileTypes->FindTileTypeFrom(t);
}

/*---------------------------FalseTerrain---------------------------*/
void Map::FalseTerrain(Tile *tile)
{
  int k;

/*Sets a tile's flag to Fake
  Then assigns a fake terrain to the tile*/

  tile->setFlags(TILE_FAKE, true);

  switch(tile->getTerrain())
  {
    case(TERRAIN_ARCTIC):
      k=(rand()%5);
      if(k<3) {tile->setFkterrain(TERRAIN_TUNDRA);}
      if(k==3){tile->setFkterrain(TERRAIN_OPENS);}
      if(k==4){tile->setFkterrain(TERRAIN_LAKE);}
      break;

    case(TERRAIN_TUNDRA):
      k=(rand()%5);
      if(k<2) {tile->setFkterrain(TERRAIN_ARCTIC);}
      if(k==2){tile->setFkterrain(TERRAIN_OPENS);}
      if(k>2) {tile->setFkterrain(TERRAIN_LAKE);}
      break;

    case(TERRAIN_DESERT):
      k=(rand()%10);
      if(k<2)      {tile->setFkterrain(TERRAIN_PRAIRIE);}
      if(k>1&&k<4) {tile->setFkterrain(TERRAIN_GRASS);}
      if(k>3&&k<6) {tile->setFkterrain(TERRAIN_STEPPE);}
      if(k==6)     {tile->setFkterrain(TERRAIN_LAKE);}
      if(k>6&&k<10){tile->setFkterrain(TERRAIN_CHAPARRAL);}
      break;

    case(TERRAIN_OPENS):
      k=(rand()%10);
      if(k<2)     {tile->setFkterrain(TERRAIN_GRASS);}
      if(k>1&&k<4){tile->setFkterrain(TERRAIN_PRAIRIE);}
      if(k>3&&k<6){tile->setFkterrain(TERRAIN_DESERT);}
      if(k>5&&k<8){tile->setFkterrain(TERRAIN_STEPPE);}
      if(k>7)     {tile->setFkterrain(TERRAIN_LAKE);}
      break;

    case(TERRAIN_SAVANNAH):
      k=(rand()%11);
      if(k<3)     {tile->setFkterrain(TERRAIN_GRASS);}
      if(k>2&&k<5){tile->setFkterrain(TERRAIN_STEPPE);}
      if(k>4&&k<7){tile->setFkterrain(TERRAIN_DESERT);}
      if(k>6&&k<9){tile->setFkterrain(TERRAIN_LAKE);}
      if(k>8)     {tile->setFkterrain(TERRAIN_CHAPARRAL);}
      break;

    case(TERRAIN_GRASS):
      k=(rand()%11);
      if(k<2)     {tile->setFkterrain(TERRAIN_PRAIRIE);}
      if(k>1&&k<5){tile->setFkterrain(TERRAIN_STEPPE);}
      if(k>4&&k<7){tile->setFkterrain(TERRAIN_SAVANNAH);}
      if(k>6&&k<9){tile->setFkterrain(TERRAIN_LAKE);}
      if(k>8)     {tile->setFkterrain(TERRAIN_DESERT);}
      break;

    case(TERRAIN_PRAIRIE):
      k=(rand()%11);
      if(k<3)     {tile->setFkterrain(TERRAIN_GRASS);}
      if(k>2&&k<5){tile->setFkterrain(TERRAIN_OPENS);}
      if(k>4&&k<7){tile->setFkterrain(TERRAIN_DESERT);}
      if(k>6&&k<9){tile->setFkterrain(TERRAIN_LAKE);}
      if(k>8)     {tile->setFkterrain(TERRAIN_CHAPARRAL);}
      break;

    case(TERRAIN_STEPPE):
      k=(rand()%11);
      if(k<3)     {tile->setFkterrain(TERRAIN_GRASS);}
      if(k>2&&k<5){tile->setFkterrain(TERRAIN_SAVANNAH);}
      if(k>4&&k<7){tile->setFkterrain(TERRAIN_DESERT);}
      if(k>6&&k<9){tile->setFkterrain(TERRAIN_LAKE);}
      if(k>8)     {tile->setFkterrain(TERRAIN_CHAPARRAL);}
      break;

    case(TERRAIN_CHAPARRAL):
      k=(rand()%11);
      if(k<3)     {tile->setFkterrain(TERRAIN_DESERT);}
      if(k>2&&k<5){tile->setFkterrain(TERRAIN_SAVANNAH);}
      if(k>4&&k<7){tile->setFkterrain(TERRAIN_PRAIRIE);}
      if(k>6&&k<9){tile->setFkterrain(TERRAIN_LAKE);}
      if(k>8)     {tile->setFkterrain(TERRAIN_GRASS);}
      break;
    default: break;
  }

  //If the tile isn't water, randomly put a fake rumor on it
  if(tile->getFkterrain() != TERRAIN_LAKE)
    if((rand()%5)==1)
      tile->setFlags(TILE_FAKERUMOR, true);

  //If the tile isn't water or arctic, randomly put a fake forest on it
  if(tile->getFkterrain() != TERRAIN_LAKE && tile->getFkterrain()!=TERRAIN_ARCTIC)
    if((rand()%10)==1)
      tile->setFlags(TILE_FAKEFOREST, true);

  //If the tile is arctic, randomly put a fake peak on it
  if(tile->getFkterrain() == TERRAIN_ARCTIC)
    if((rand()%5)==1)
      tile->setFlags(TILE_FAKEPEAK, true);

  //If the tile is tundra, randomly put a fake mountain on it
  if(tile->getFkterrain() == TERRAIN_TUNDRA)
    if((rand()%5)==1)
      tile->setFlags(TILE_FAKEMOUNTAIN, true);

  //If the tile is opens, randomly put a fake hill on it
  if(tile->getFkterrain() == TERRAIN_OPENS)
    if((rand()%5)==1)
      tile->setFlags(TILE_FAKEHILL, true);
}

/*--------------------------ClearForest---------------------------*/
bool Map::ClearForest(Tile *tile)
{
  //  Clear the Forest flag for the tile, removing the link flags
  //  for the tile. Then update the links for the 4 connecting tiles.

  if(!tile) return false;
  
  int x = tile->getXPos(), y = tile->getYPos();

  tile->setFlags(TILE_FOREST, false);

  //update surrounding links
  UpdateTileLinks(TILE_FOREST, x  , y-1); //N
  UpdateTileLinks(TILE_FOREST, x+1, y  ); //E
  UpdateTileLinks(TILE_FOREST, x  , y+1); //S
  UpdateTileLinks(TILE_FOREST, x-1, y  ); //W
  return true;
  //TO DO: add logged wood to nearest colony
  //Will be done in the Order() function
}


/*---------------------------PlaceRoad----------------------------*/

void Map::PlaceRoad(Tile *tile)
{
  //  Set the Road flag for the tile, then set the link flags for
  //  the tile and the links for the 8 connecting tiles.

  if(!tile) return;
  
  int x = tile->getXPos(), y = tile->getYPos();
  tile->setFlags(TILE_ROAD, true);
  UpdateTileLinks(TILE_ROAD, x  , y  );

  UpdateTileLinks(TILE_ROAD, x-1, y-1); //NW
  UpdateTileLinks(TILE_ROAD, x  , y-1); //N
  UpdateTileLinks(TILE_ROAD, x+1, y-1); //NE
  UpdateTileLinks(TILE_ROAD, x+1, y  ); //E
  UpdateTileLinks(TILE_ROAD, x+1, y+1); //SE
  UpdateTileLinks(TILE_ROAD, x  , y+1); //S
  UpdateTileLinks(TILE_ROAD, x-1, y+1); //SW
  UpdateTileLinks(TILE_ROAD, x-1, y  ); //W
}

/*------------------------UpdateTileLinks-------------------------*/
void Map::UpdateTileLinks(enum tileTypes type, int x, int y)
{
    long ii = y*width + x;

    //test if tile indices are out of bounds
    if(x<0 || x>width-1 || y<0 || y>height-1) return;

    switch(type)
    {
    case TILE_ROAD: //Links in 8 directions
        if(!tiles[ii]->Is(TILE_ROAD)) return;
        if(x>0)
        {
            tiles[ii]->setLink(type,W,getTileAt(x-1,y)->Is(type));
            if(y>0) tiles[ii]->setLink(type,NW,getTileAt(x-1,y-1)->Is(type));
            if(y<height-1) tiles[ii]->setLink(type,SW,getTileAt(x-1,y+1)->Is(type));
        }
        if(x<width-1)
        {
            tiles[ii]->setLink(type,E,getTileAt(x+1,y)->Is(type));
            if(y>0) tiles[ii]->setLink(type,NE,getTileAt(x+1,y-1)->Is(type));
            if(y<height-1) tiles[ii]->setLink(type,SE,getTileAt(x+1,y+1)->Is(type));
        }
        if(y>0) tiles[ii]->setLink(type,N,getTileAt(x,y-1)->Is(type));
        if(y<height-1) tiles[ii]->setLink(type,S,getTileAt(x,y+1)->Is(type));
        break;

    case TILE_PEAK    : case TILE_FAKEPEAK    : //links in 4 directions
    case TILE_MOUNTAIN: case TILE_FAKEMOUNTAIN: // N, E, S and W
    case TILE_HILL    : case TILE_FAKEHILL    :
    case TILE_FOREST  : case TILE_FAKEFOREST  :
        if(x>0)
        {
            tiles[ii]->setRealLink(type,W,getTileAt(x-1,y)->IsReal(type));
            tiles[ii]->setFakeLink(type,W,getTileAt(x-1,y)->IsFake(type));
        }
        if(x<width-1)
        {
            tiles[ii]->setRealLink(type,E,getTileAt(x+1,y)->IsReal(type));
            tiles[ii]->setFakeLink(type,E,getTileAt(x+1,y)->IsFake(type));
        }
        if(y>0)
        {
            tiles[ii]->setRealLink(type,N,getTileAt(x,y-1)->IsReal(type));
            tiles[ii]->setFakeLink(type,N,getTileAt(x,y-1)->IsFake(type));
        }
        if(y<height-1)
        {
            tiles[ii]->setRealLink(type,S,getTileAt(x,y+1)->IsReal(type));
            tiles[ii]->setFakeLink(type,S,getTileAt(x,y+1)->IsFake(type));
        }
        break;

    case TILE_RIVER:
        if(!tiles[ii]->Is(TILE_RIVER)) return;
        if(x>0)
        {
            tiles[ii]->setLink(type,W,getTileAt(x-1,y)->Is(type)||
                                      getTileAt(x-1,y)->Is(TILE_WATER));
            if(y>0) tiles[ii]->setLink(type,NW,getTileAt(x-1,y-1)->Is(type)||
                                               getTileAt(x-1,y-1)->Is(TILE_WATER));
            if(y<height-1) tiles[ii]->setLink(type,SW,getTileAt(x-1,y+1)->Is(type)||
                                               getTileAt(x-1,y+1)->Is(TILE_WATER));
        }
        if(x<width-1)
        {
            tiles[ii]->setLink(type,E,getTileAt(x+1,y)->Is(type)||
                                      getTileAt(x+1,y)->Is(TILE_WATER));
            if(y>0) tiles[ii]->setLink(type,NE,getTileAt(x+1,y-1)->Is(type)||
                                               getTileAt(x+1,y-1)->Is(TILE_WATER));
            if(y<height-1) tiles[ii]->setLink(type,SE,getTileAt(x+1,y+1)->Is(type)||
                                                      getTileAt(x+1,y+1)->Is(TILE_WATER));
        }
        if(y>0) tiles[ii]->setLink(type,N,getTileAt(x,y-1)->Is(type)||
                                          getTileAt(x,y-1)->Is(TILE_WATER));
        if(y<height-1) tiles[ii]->setLink(type,S,getTileAt(x,y+1)->Is(type)||
                                                 getTileAt(x,y+1)->Is(TILE_WATER));
        break;

    default:
        break;
    }
}

int Map::getXPosFromTile(long tile)
{
  return (tile % width);
}

int Map::getYPosFromTile(long tile)
{
  return (tile / height);
}

bool Map::IsOnScreen(long tile)
{
  if((tile>=0) && (tile<tiles.size())) return (tiles[tile]->getScreenTile()!=-1);
  return false;
}

long Map::getTileFromPos(int x, int y)
{
  if(x>=0 && x<width && y>=0 && y<height) return (y * width + x);
  return -1;
}

//
// $Id$
//
