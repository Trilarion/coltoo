/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
//
// FILE		: Indian.h
//
// VERSION	: 0.1
//
// AUTHOR	: Chainsword,  June 2004
//
// CLASS	: Indian
//
// DESCRIPTION	: Basis class for indian control.
//

#ifndef INDIAN_H
#define INDIAN_H

#include <vector>
#include <string>

#include "ai.h"
#include "unit.h"
#include "Village.h"

using namespace std;

class AI;
class Unit;
class Village;

class IndianLevel
{
public:
    IndianLevel();
    virtual ~IndianLevel();
    
    //GET Methods
    int LevelID();
    char *Type();
    char *CityName();
    char *CityPlural();
    
    //SET Methods
    void LevelID(int id);
    void Type(const char *n);
    void CityName(const char *n);
    void CityPlural(const char *n);
    
private:
    int levelID;
    string levelType;
    string levelCityName;
    string levelCityPlural;
};

class Indian : public Actor
{
public:

//Constructor, destructor
    Indian();
    virtual ~Indian();
    virtual bool Init(void);
    virtual bool InitAI(void);
    
//GET Methods
    char *getPlural();
    char *getResources();
    int getTechLevel();
    Unit *getUnit(int villageID, int unitID);
    Village *getVillage(int villageID);
    Village *getVillage(long tile);
    int getHomeVillage();
    int getNumVillages();
    int getMaxVillages();
    Uint8 getIndianID();
    AI *getAI();
    
//SET Methods
    void setPlural(const char *n);
    void setResources(const char *r);
    void setTechLevel(int level);
    void setHomeVillage(int villageID);
    void setMaxVillages(int maxVillages);
    void setIndianID(int indianID);
    
//ADD Methods
    void addVillage(Village *v);
    
//DESTROY Methods
    void destroyVillage(int villageID);
    
private:
    string plural;
    string resources;
    int techLevel;
    int homeVillage;
    int numVillages;
    vector<Village*> villages;
    AI *ai;
};
#endif

