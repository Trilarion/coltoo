/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
// FILE		: Indians.cpp
//
// VERSION	: 0.1
//
// AUTHOR	: Chainsword,  June 2004
//
// CLASS	: Indians
//
// DESCRIPTION	: class for indians.
//

#include "Indians.h"
#include "map.h"

Indians::Indians()
{
}

Indians::Indians(GameEngine *g) : m_pGame(g)
{
}

Indians::~Indians()
{
    vector<Indian*>::iterator it;
    for( it = indians.begin(); it != indians.end(); it++ )
    {
        if (*it) delete (*it);
    }
    indians.clear();
    vector<IndianLevel*>::iterator il;
    for( il = indianLevels.begin(); il != indianLevels.end(); il++ )
    {
        if (*il) delete (*il);
    }
    indianLevels.clear();
}

bool Indians::Init()
{
    #ifdef DEBUG
    cout<<"    Loading Tribes {"<<endl;
    #endif
    LoadTribes("data/text/tribes.xml");
    #ifdef DEBUG
    cout<<"    }"<<endl;
    #endif
    if(indians.size()) return true;
    return false;
}

Indian *Indians::getIndian(int iID)
{
    if((iID>=0) && (iID<indians.size())) return indians[iID];
    return NULL;
}

int Indians::getNumIndians()
{
    return indians.size();
}

void Indians::LoadTribes(const char *fn)
{
    TiXmlDocument doc(fn);
    TiXmlBase::SetCondenseWhiteSpace(false);
    bool ok = doc.LoadFile();
    if(ok) //Read tribes.xml and place indian villages and warriors
    {
        #ifdef DEBUG
        cout<<"      Loading from "<<fn<<endl;
        #endif
        TiXmlNode *n = doc.FirstChild("indian");
        InitIndianLevels(n->FirstChild("levels"));
        InitIndians(n->FirstChild("tribes"));
    }
    else //Use the old method to place the indian villages and warriors
    {
        #ifdef DEBUG
        cout<<"      Creating new tribes"<<endl;
        #endif
        CreateIndians();
        CreateVillages();
    }
}

void Indians::InitIndians(TiXmlNode *node)
{
    TiXmlNode *n = node->FirstChild("tribe");
    TiXmlElement *e;
    Indian *i;
    int v;
    #ifdef DEBUG
    cout<<"      Tribes {"<<endl;
    #endif
    while(n)
    {
        e = n->ToElement();
        i = new Indian();
        i->setIndianID(NATION_TRIBE + indians.size());
        if(e->Attribute("name")) i->setName(e->Attribute("name"));
        if(e->Attribute("plural")) i->setPlural(e->Attribute("plural"));
        if(e->Attribute("resources")) i->setResources(e->Attribute("resources"));
        e->Attribute("villages", &v);
        i->setMaxVillages(v);
        e->Attribute("tech", &v);
        i->setTechLevel(v);
        if(e->Attribute("color")) i->Color->SetColor(e->Attribute("color"));
        #ifdef DEBUG
        cout<<"        Tribe "<<i->getName()<<endl;
        #endif
        CreateVillages(i);
        indians.push_back(i);
        n = n->NextSibling("tribe");
    }
    #ifdef DEBUG
    cout<<"      }"<<endl;
    #endif
    i = NULL;
}

void Indians::InitIndianLevels(TiXmlNode *node)
{
    TiXmlNode *n = node->FirstChild("level");
    TiXmlElement *e;
    IndianLevel *l;

    #ifdef DEBUG
    cout<<"      Tribe Levels {"<<endl;
    #endif
    while(n)
    {
        e = n->ToElement();
        l = new IndianLevel();
        l->LevelID(indianLevels.size());
        l->Type(e->Attribute("type"));
        l->CityName(e->Attribute("cityName"));
        l->CityPlural(e->Attribute("cityPlural"));
        #ifdef DEBUG
        cout<<"        Tribe Level "<<l->Type()<<endl;
        #endif
        indianLevels.push_back(l);
        n = n->NextSibling("level");
    }
    #ifdef DEBUG
    cout<<"      }"<<endl;
    #endif
    l = NULL;
}

void Indians::CreateIndians()
{
    int i;
    Indian *ind;
    for(i=0; i<4; i++)
    {
        ind = new Indian();
        ind->setIndianID(NATION_TRIBE + i);
        ind->setTechLevel(i);
        indians.push_back(ind);
    }
}

void Indians::CreateVillages(Indian *ind)
{
    Map *map = m_pGame->GetMap();
    int i, count_test;
    Tile *tile;
    if(!ind || !map) return;
    #ifdef DEBUG
    cout<<"        Number of Villages: "<<ind->getMaxVillages()<<endl;
    #endif
    for(i=0; i<ind->getMaxVillages(); i++)
    {
        bool placed = false;
        count_test = 20;
        while(!placed)
        {
            int x = rand() % (map->GetWidth()-1) + 1, y = rand() % (map->GetHeight()-1) + 1;
            tile = map->getTileAt(x, y);
            if(!tile) break;
            if(!tile->Is(TILE_WATER) && !tile->Is(TILE_PEAK) &&!tile->Is(TILE_MOUNTAIN) && !tile->Is(TILE_RUMOR))
            {
                int xx, yy, landTiles = 0;
                bool hasVillages = false;
                for(yy=y-1; yy<y+2; yy++)
                {
                    for(xx=x-1; xx<x+2; xx++)
                    {
                        Tile *tile = map->getTileAt(xx, yy);
                        if(tile)
                        {
                            if(!tile->Is(TILE_WATER) && !tile->Is(TILE_PEAK)) landTiles++;
                            hasVillages |= (map->getTileAt(xx, yy)->getVillages() != 0);
                            if(hasVillages) break;
                        }
                    }
                    if(hasVillages) break;
                }
                #ifdef DEBUG
                cout<<"        HasVillages: "<<hasVillages<<";  Land Tiles: "<<landTiles<<endl;
                #endif
                if(!hasVillages && (landTiles>1))
                {
                    placed = true;
                    if(ind)
                    {
                        Village *v = new Village(m_pGame);
                        if(v)
                        {
                            ind->addVillage(v);
                            if(ind->getNumVillages()==1) ind->setHomeVillage(v->ID());
                            v->setTile(y * map->GetWidth() + x);
                            tile->setVillages(ind->getTechLevel() + 1);
                            v->InitUnits(ind->getIndianID(), ind->getTechLevel());
                        }
                        else placed = true;
                    }
                    
                }
            }
            if(!placed) count_test--;
            if(count_test<=0) placed = true;
        }
    }
}

void Indians::CreateVillages()
{
    int x, y, ii;
    Map *map = m_pGame->GetMap();
    Tile *tile;
    bool hasVillages;
    int xx, yy, landTiles;
    #ifdef DEBUG
    cout<<"      Old Method of Village creating"<<endl;
    #endif
    for(y=1, ii=map->GetWidth()+1; y < map->GetHeight()-1; y++)
    {
        for(x=1; x < map->GetWidth()-1; x++, ii++)
        {
            tile = map->getTile(ii);
            if(!tile->Is(TILE_WATER) && !tile->Is(TILE_PEAK) && !tile->Is(TILE_MOUNTAIN) && (tile->getTerrain()!=TERRAIN_ARCTIC) && !tile->Is(TILE_RUMOR))
            {
                int k = rand() % 7;
                if(k==1)
                {
                    hasVillages = false;
                    landTiles = 0;
                    for(yy=y-1;yy<y+2;yy++)
                    {
                        for(xx=x-1;xx<x+2;xx++)
                        {
                            Tile *tl = map->getTileAt(xx, yy);
                            if(!tl->Is(TILE_WATER) && !tl->Is(TILE_PEAK)) landTiles++;
                            hasVillages |= (tl->getVillages() != 0);
                            if(hasVillages) break;
                        }
                        if(hasVillages) break;
                    }
                    if(!hasVillages && (landTiles>1))
                    {
                        Uint8 tl = rand() % 4;
                        Indian *i = getIndian(tl);
                        if(i)
                        {
                            Village *v = new Village();
                            i->addVillage(v);
                            if(i->getNumVillages()==1) i->setHomeVillage(v->ID());
                            v->setTile(ii);
                            tile->setVillages(tl + 1);
                            v->InitUnits(i->getIndianID(), i->getTechLevel());
                        }
                    }
                }
            }
        }
    }
}

