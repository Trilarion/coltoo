/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <cstring>               // for strlen()

#include "graphicsEngine.h"
#define MAX_BUFFER 1023
#define SIZEOF_XPOS 128

#ifdef _MSC_VER
// required for Visual C++, as it isn't C99-compliant
// TODO: what's with Borland?
#define vsnprintf _vsnprintf
#endif

// drawString and drawString2 are almost identical, just one uses TrueType,
// and another one raster fonts - rename?
/*---------------------------drawString----------------------------*/
void GraphicsEngine::drawString(SDL_Surface *screen, SDLFont *font, int x, int y,
                                bool shadow, char *format, ...)
{
	char buffer[MAX_BUFFER + 1];

	va_list ap;
    va_start(ap, format);
    vsnprintf(buffer, MAX_BUFFER, format, ap);
    va_end(ap);
    if(!*buffer) return;
    buffer[strlen(buffer) - 1] = 0;

	if(shadow)
    {
		SDL_Color black = { 0x00, 0x00, 0x00, 0 };
		blitString(screen, font->font, x + 1, y + 1, buffer, black);
	}

	blitString(screen, font, x, y, buffer);
}

/*---------------------------drawString2--------------------------*/
void GraphicsEngine::drawString2(SDL_Surface *screen, SDL_Surface *font,
                                 int x, int y, char *format, ...)
{
    char string[MAX_BUFFER + 1];
    // TODO: why 128?
    int xpos[SIZEOF_XPOS], ypos = 1;

    va_list ap;
    va_start(ap, format);
    vsnprintf(string, MAX_BUFFER, format, ap);
    va_end(ap);

    for(int i = 0, xx = 1; i < SIZEOF_XPOS; i++, xx += 7)
    {
        xpos[i] = xx;
    }
    
    for(int i = 0, xx = 0; string[i]; i++, xx += 7)
    {  
        DrawTILE(screen, font, xx + x, y, 6, 11, xpos[string[i]], ypos);
    }
}

/*---------------------------stringWidthSoL1---------------------------*/
int GraphicsEngine::stringWidthSoL1(char *format, ...)
{
    char str[MAX_BUFFER + 1];

    va_list ap;
    va_start(ap, format);
    vsnprintf(str, MAX_BUFFER, format, ap);
    va_end(ap);
    if(!*str) return 0;

    return 7 * strlen(str) - 1;
}

/*---------------------------drawString3---------------------------*/
// TODO: too many parameters; why are the colors Uint32?
void GraphicsEngine::drawString3(SDL_Surface *screen, SDL_Surface *font, int x,
                                 int y, Uint32 r1, Uint32 g1, Uint32 b1,
                                 Uint32 r2, Uint32 g2, Uint32 b2, char *format, ...)
{
    char str[MAX_BUFFER + 1];
    int xpos[SIZEOF_XPOS], ypos = 1;

    va_list ap;
    va_start(ap, format);
    vsprintf(str, format, ap);
    va_end(ap);

    SDL_Surface *temp = SetFontColor(font, r1, g1, b1, r2, g2, b2);

    for(int i = 0, xx = 1; i < SIZEOF_XPOS; i++, xx += 7)
    {
        xpos[i] = xx;
    }

    for(int i = 0, xx = 0; str[i]; i++, xx += 7)
    {  
        DrawTILE(screen, temp, xx + x, y, 6, 11, xpos[str[i]], ypos);
    }

    SDL_FreeSurface(temp);
}

/*---------------------------SetFontColor-------------------------*/
// Warning! You must manually free the returned surface
//
SDL_Surface* GraphicsEngine::SetFontColor(SDL_Surface *font,Uint32 r1,Uint32 g1,
                                        Uint32 b1,Uint32 r2,Uint32 g2,Uint32 b2)
{
    SDL_Surface *temp = SDL_CreateRGBSurface(SDL_SWSURFACE, font->w, font->h,
                                           font->format->BitsPerPixel,
                                           font->format->Rmask,
                                           font->format->Gmask,
                                           font->format->Bmask,
                                           font->format->Amask);

    FillSurface(temp, 255, 0, 255);               
    DrawSURF(temp, font, 0, 0);

    Uint32 yellow, black;
    yellow = SDL_MapRGB(temp->format, 255, 255, 0);
	black = SDL_MapRGB(temp->format, 0, 0, 0);

    /*lock the surface*/	
    if(SDL_MUSTLOCK(temp))
        SDL_LockSurface(temp);

    for(int q = 0; q < temp->h; q++)
    {
        for(int w = 0; w < temp->w; w++)
        {
            Uint32 ret = GetPixel(temp, w, q);
            
            if(ret == yellow)
                DrawPixel(temp, w, q, r1, g1, b1);
            else if(ret == black)
                DrawPixel(temp, w, q, r2, g2, b2);
        }
    }

    /*Unlock the surface*/	
    if(SDL_MUSTLOCK(temp))
        SDL_UnlockSurface(temp);

    return temp;
}

//
// $Id$
//

