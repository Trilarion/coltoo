/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "graphicsEngine.h"
#include "map.h"

/*---------------------------ReCenter-----------------------------*/
void GraphicsEngine::ReCenter(void)
{
  GlobalData* data = m_pGame->GetData();
  Map* map = m_pGame->GetMap();
  int oldscrnum = data->scrnum; //added 08/07; fixes SlideUnit bug
  int x, y, ii = data->start;
  
  #ifdef DEBUG
  cout<<"Recentering screen"<<endl;
  #endif
  for(y=0; y<screenHeight; y++, ii+=map->GetWidth())
  {
    for(x=0; x<screenWidth; x++)
    {
      if(map->getTile(ii+x)) map->getTile(ii+x)->setScreenTile(-1);
    }
  }
/*Recenters the unit with checks to see if the map edge or corners
  have been reached.*/
  data->scrnum=71;data->start=data->mapnum-6*map->GetWidth()-5;

/*If the unit is within 5 rows of the North or South map edge the 
  number of rows between it and the START tile is set. 
  (150 tiles/row)*/

  if((map->getTile(data->mapnum)->getYPos()<6)||
     (map->getTile(data->mapnum)->getYPos()>map->GetHeight()-8))
  {
    //changed 06/07; using map->GetHeight() instead of hardcoded values
    int yPos = map->getTile(data->mapnum)->getYPos();
    if((yPos>map->GetHeight()-8)&&(yPos<map->GetHeight()))
    {
      data->scrnum = 159 - 11 * (map->GetHeight()-yPos);
      data->start = data->mapnum -((14-(map->GetHeight()-yPos)) * map->GetWidth() + 5);
    }
    else if((yPos>=0)&&(yPos<5))
    {
      data->scrnum = 5 + 11 * yPos;
      data->start = data->mapnum - (yPos * map->GetWidth() + 5);
    }
    else
    {
      data->scrnum = 5;
      data->start = data->mapnum - (map->GetWidth() + 5);
    }
  }

/*If the unit is within 5 tiles of the West edge, or 4 tiles of the
  East edge, the number of columns from the START tile is set.*/   
  if((map->getTile(data->mapnum)->getXPos()<5)||
     (map->getTile(data->mapnum)->getXPos()>map->GetWidth()-6))
  {
    //changed 06/07; using map->GetWidth() instead of hardcoded values
    int xPos = map->getTile(data->mapnum)->getXPos();
    if((xPos>map->GetWidth()-6)&&(xPos<map->GetWidth()))
    {
      data->scrnum += 6-(map->GetWidth()-xPos);
      data->start = data->mapnum-(6*map->GetWidth()+11-(map->GetWidth()-xPos));
    }
    else if((xPos>0)&&(xPos<5))
    {
      data->scrnum -= 5-xPos;
      data->start = data->mapnum - (6*map->GetWidth()+xPos);
    }
    else
    {
      data->scrnum -=5;
      data->start = data->mapnum - (6*map->GetWidth());
    }
  }

/*If the unit is in one of the 4 corners, the distance from the 
  START tile is set.*/
  if((map->getTile(data->mapnum)->getXPos()<5)&&
     (map->getTile(data->mapnum)->getYPos()<6))
  {
    #ifdef DEBUG
    cout<<"x<5 && y<6"<<endl;
    #endif
      data->start=data->mapnum-((int(data->scrnum/11)*map->GetWidth())+
                                (map->getTile(data->mapnum)->getXPos()));
  }
  if((map->getTile(data->mapnum)->getXPos()<5)&&
     (map->getTile(data->mapnum)->getYPos()>map->GetHeight()-8))
  {
    #ifdef DEBUG
    cout<<"x<5 && y>height-8"<<endl;
    #endif
      data->start=data->mapnum-((int(data->scrnum/11)*map->GetWidth())+
                                 map->getTile(data->mapnum)->getXPos());
  }
  if((map->getTile(data->mapnum)->getXPos()>map->GetWidth()-6)&&
     (map->getTile(data->mapnum)->getYPos()<5))
  {
    #ifdef DEBUG
    cout<<"x>width-6 && y<5"<<endl;
    #endif
      data->start=map->GetWidth()-11;
  }
  if((map->getTile(data->mapnum)->getXPos()>map->GetWidth()-6)&&
     (map->getTile(data->mapnum)->getYPos()>map->GetHeight()-8))
  {
    #ifdef DEBUG
    cout<<"x>width-6 && y>height-8"<<endl;
    #endif
      data->start=(map->GetHeight()-13)*map->GetWidth()-11;
  } /**/

  //added 08/07; updates the oldloc for SlideUnit
  data->oldloc += data->scrnum-oldscrnum;
  data->flag|=data->ReCenFlag;
  
  ii = data->start;
  for(y=0; y<screenHeight; y++, ii+=map->GetWidth())
  {
    for(x=0; x<screenWidth; x++)
    {
      map->getTile(ii+x)->setScreenTile(getScreenTileAt(x,y));
    }
  }
}

/*---------------------------ReCenter-----------------------------*/
void GraphicsEngine::ReCenter(int x, int y)
{
  GlobalData* data = m_pGame->GetData();
  Map* map = m_pGame->GetMap();

  //update the start tile, checking map borders.
  if(x < 0 || x >= map->GetWidth() || y<0 || y>= map->GetHeight())
    cout << "Recenter Error: indices out of range\n";
  else
  {
    int coordX = x - GetScreenWidth()/2;
    if(coordX < 0)
    {
      startX = 0;
    }
    else if(coordX > map->GetWidth()- GetScreenWidth())
         {
           startX = map->GetWidth()-GetScreenWidth();
         }
         else startX = coordX;

    int coordY = y - GetScreenHeight()/2;
    if(coordY < 0)
    {
      startY = 0;
    }
    else if(coordY > map->GetHeight()- GetScreenHeight())
         {
           startY = map->GetHeight()-GetScreenHeight();
         }
         else startY = coordY;

    data->flag&=~data->ReCenFlag;
  }
}

//
// $Id$
//

