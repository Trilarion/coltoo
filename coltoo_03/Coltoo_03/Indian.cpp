/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
// FILE		: Indian.cpp
//
// VERSION	: 0.1
//
// AUTHOR	: Chainsword,  June 2004
//
// CLASS	: Indian
//
// DESCRIPTION	: Basis class for indian control.
//

#include "Indian.h"

IndianLevel::IndianLevel()
{
    levelID = 0;
    levelType = "[ANY]";
    levelCityName = "Capital";
    levelCityPlural = "Capitals";
}
IndianLevel::~IndianLevel()
{
}

//GET Methods
int IndianLevel::LevelID()
{
    return levelID;
}

char *IndianLevel::Type()
{
    return (char *)levelType.c_str();
}

char *IndianLevel::CityName()
{
    return (char *)levelCityName.c_str();
}

char *IndianLevel::CityPlural()
{
    return (char *)levelCityPlural.c_str();
}

//SET Methods
void IndianLevel::LevelID(int ID)
{
    levelID = ID;
}

void IndianLevel::Type(const char *t)
{
    levelType = t;
}

void IndianLevel::CityName(const char *n)
{
    levelCityName = n;
}

void IndianLevel::CityPlural(const char *p)
{
    levelCityPlural = p;
}

Indian::Indian()
{
    name = "Indian";
    plural = "Indians";
    resources = "ANY";
    techLevel = 0;
    numVillages = -1;
    ai = NULL;
    Color = new ColorData();
    Color->SetColor("160, 128, 0");
}

Indian::~Indian()
{
    if(ai) delete(ai);
    ai = NULL;
    if(Color) delete(Color);
    Color = NULL;
    vector<Village*>::iterator it = villages.begin();
    for( it = villages.begin(); it != villages.end(); it++ )
    {
        if (*it) delete (*it);
    }
    villages.clear();
}

bool Indian::Init()
{
    return true;
}

bool Indian::InitAI()
{
    return true;
}

//GET Methods
Uint8 Indian::getIndianID()
{  
    return actorID;
}
char *Indian::getPlural()
{
    return (char *)plural.c_str();
}

char *Indian::getResources()
{
    return (char *)resources.c_str();
}

int Indian::getTechLevel()
{
    return techLevel;
}

int Indian::getHomeVillage()
{
    return homeVillage;
}

int Indian::getNumVillages()
{
    return villages.size();
}

int Indian::getMaxVillages()
{
    return numVillages;
}

Unit *Indian::getUnit(int vID, int uID)
{
    if((vID>=0) && (vID<villages.size())) return villages[vID]->getUnit(uID);
    return NULL;
}

Village *Indian::getVillage(int vID)
{
    if((vID>=0) && (vID<villages.size())) return villages[vID];
    return NULL;
}

Village *Indian::getVillage(long t)
{
    int v;
    for(v=0; v<villages.size(); v++)
    {
        if(villages[v]->getTile() == t) return villages[v]; 
    }
    return NULL;
}

AI *Indian::getAI()
{
    return ai;
}

//SET Methods
void Indian::setIndianID(int iID)
{
    actorID = iID;
}

void Indian::setPlural(const char *p)
{
    plural = p;
}

void Indian::setResources(const char *r)
{
    resources = r;
}

void Indian::setTechLevel(int l)
{
    techLevel = l;
}

void Indian::setHomeVillage(int vID)
{
    if((vID>=0) && (vID<villages.size())) homeVillage = vID;
}

void Indian::setMaxVillages(int nv)
{
    numVillages = nv;
}

//ADD Methods
void Indian::addVillage(Village *v)
{
    if(v)
    {
        if(numVillages==-1) villages.push_back(v);
        else if(numVillages>villages.size()) villages.push_back(v);
    }
}

//DESTROY Methods
void Indian::destroyVillage(int vID)
{
    if((vID>=0) && (vID<villages.size()))
    {
        if(villages[vID])
        {
            delete(villages[vID]);
            villages[vID] = villages[villages.size()-1];
            villages.pop_back();
        }
    }
}

