/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <SDL/SDL.h>

#include "message.h"
#include "graphicsEngine.h"
#include "map.h"

/*---------------------------MessageWindow-----------------------*/
// TODO: MessageWindow, ShowSysMessage and DisplayMessage
// should be in the Message class
void GraphicsEngine::MessageWindow(int msgID, int sysID)
{
    GlobalData *data = m_pGame->GetData();
    int x0 = 0, y0 = 0, xM, yM;
    SDLMessage keep;
    
    if(msgID == MSG_SYSTEM)
    {
        keep = ShowSysMessage(sysID);
        UpdateScreen();

        /*wait for a mouse click or the return key*/  
        while(SDL_WaitEvent(&data->event))
        {
            if(SDL_GetMouseState(&xM,&yM)&SDL_BUTTON(1))
            {
                break;
            }
            if ( data->event.type == SDL_KEYDOWN )
            {
                if (data->event.key.keysym.sym == SDLK_RETURN ||
                    data->event.key.keysym.sym == SDLK_KP_ENTER ||
                    data->event.key.keysym.sym == SDLK_ESCAPE)
                    break;
                }
        }

        DrawSURF(screen, keep.bck, keep.pos.x, keep.pos.y);
        UpdateScreen();
    }
    else
    {
        //else show normal MessageWindow
        MessageWindow(msgID);
    }
}

//added 26/6; just show some possible statistics
void GraphicsEngine::MessageWindow(int k, Unit *att, Unit *def)
{
    GlobalData *data = m_pGame->GetData();
    int xM,yM; //Coordinates of mouse pointer
		  int x0 = 0, y0 = 0;
    SDLMessage keep;
    
    if(k==MSG_COMBAT)
    {
        #ifdef DEBUG
        cout<<"Combat Analysis Message"<<endl;
        #endif
        //if it's a combat message show special SYS_COMBAT
        keep = ShowSysMessage(SYS_COMBAT, att, def);
        UpdateScreen();

        /*wait for a mouse click or the return key*/  
        while(SDL_WaitEvent(&data->event))
        {
            if(SDL_GetMouseState(&xM,&yM)&SDL_BUTTON(1))
            {
                if(k<2 || k>2){break;}
                else
                {
                    /* if message is a village, click must be on box*/
                    if(xM>x0&&xM<x0+400&&yM>y0&&yM<y0+150&&k==2){break;}
                }          
            }
            if ( data->event.type == SDL_KEYDOWN )
            {
                if (data->event.key.keysym.sym == SDLK_RETURN ||
                    data->event.key.keysym.sym == SDLK_KP_ENTER ||
                    data->event.key.keysym.sym == SDLK_ESCAPE)
                    break;
                }
        }

        DrawSURF(screen, keep.bck, keep.pos.x, keep.pos.y);
        UpdateScreen();
    }
    else
    {
        //else show normal MessageWindow
        MessageWindow(k);
    }
}

void GraphicsEngine::MessageWindow(int k)
{
    GlobalData* data = m_pGame->GetData();
    Map* map = m_pGame->GetMap();
    int xM,yM,x0,y0;
	string text;

    /*Display the message window
    Then clear it on a mouse click or return key*/

//modified 6/1

	x0 = ( screenResX / 2 ) - messagenew->w /2;
	y0 = ( screenResY / 2 ) - messagenew->h / 2;
    DrawTILE(messageold,screen,0,0,messagenew->w,messagenew->h,x0,y0);
    
    switch (k)
    {
      case MSG_RUMOR:
      {
        text = data->msg->GetRumorMessage(map->getTile(data->RumorFlag));
        DisplayMessage(text, fontInformation);
        break;
      }
      case MSG_NEWRECRUIT:
      {
      /*for proper English. If skill starts with a vowel use 'an' instead of 'a'*/
        if(data->Jobs[data->recruit[0]][0]=='A'||
              data->Jobs[data->recruit[0]][0]=='E'||
              data->Jobs[data->recruit[0]][0]=='I'||
              data->Jobs[data->recruit[0]][0]=='O'||
              data->Jobs[data->recruit[0]][0]=='U')
        {
				  text = "An ";
        }
        else
        {
				  text = "A ";
        }
				text.append(data->Jobs[data->recruit[0]]);
				text = text.substr(0,text.size()-1); //Remove the carriage return
				text += " is now Waiting on the Docks.";
				 
				DisplayMessage(text,fontInformation);
        break;
      }
      case MSG_VILLAGE:
      {
        text = data->msg->GetVillageMessage(map->getTile(data->VillageFlag));
        DisplayMessage(text, fontInformation);
        break;
      }
      case MSG_NOROOMPASS:
      {
        ShowSysMessage(SYS_NOROOMPASS);
        break;
      }
      case MSG_NOROOMCARGO:
      {
        ShowSysMessage(SYS_NOROOMCARGO);
        break;
      }
//not implemented yet
      case MSG_NOTIMPLEMENTED:
      {
        ShowSysMessage(SYS_NOTIMPLEMENTED);
        break;
      }
      case MSG_TIMESCALE:
      {
        ShowSysMessage(SYS_TIMESCALE);
        break;
      }                         
      case MSG_CANTATTACK:
      {
        ShowSysMessage(SYS_CANTATTACK);
        break;
      }                         
      case MSG_COMBAT:
      {
        ShowSysMessage(SYS_COMBAT);
        break;
      }                         
      case MSG_ENDOFTURN:
      {
        ShowSysMessage(SYS_ENDOFTURN);
        break;
      }                         
      case MSG_NOCDS:
      {
        ShowSysMessage(SYS_NOCDS);
        break;
      }
      case MSG_WRONGSKILL:
        ShowSysMessage(SYS_WRONGSKILL);
        break;
      default : {return;}
    }

    UpdateScreen();

    /*wait for a mouse click or the return key*/  
    while(SDL_WaitEvent(&data->event))
    {
        if(SDL_GetMouseState(&xM,&yM)&SDL_BUTTON(1))
        {
            if(k<2 || k>2){break;}
            else
            {
                /* if message is a village, click must be on box*/
                if(xM>x0&&xM<x0+400&&yM>y0&&yM<y0+150&&k==2){break;}
            }          
        }
        if ( data->event.type == SDL_KEYDOWN )
        {
            if (data->event.key.keysym.sym == SDLK_RETURN ||
                data->event.key.keysym.sym == SDLK_KP_ENTER ||
                data->event.key.keysym.sym == SDLK_ESCAPE)
                break;
        }
    }

    DrawSURF(screen,messageold,x0,y0);
    UpdateScreen();
}

void GraphicsEngine::MessageWindow(int msgID, long skillID)
{
  GlobalData *data = m_pGame->GetData();
  int xM,yM; //Coordinates of mouse pointer
  int x0 = 0, y0 = 0;
  SDLMessage keep;

  if(msgID==MSG_WRONGSKILL)
  {
    //if it's a wrong skill message show special SYS_WRONGSKILL
    keep = ShowSysMessage(SYS_WRONGSKILL, skillID);
    UpdateScreen();

    /*wait for a mouse click or the return key*/
    while(SDL_WaitEvent(&data->event))
    {
      if(SDL_GetMouseState(&xM,&yM)&SDL_BUTTON(1)) break;
      if ( data->event.type == SDL_KEYDOWN )
      {
        if (data->event.key.keysym.sym == SDLK_RETURN ||
            data->event.key.keysym.sym == SDLK_KP_ENTER ||
            data->event.key.keysym.sym == SDLK_ESCAPE)
          break;
      }
    }

    DrawSURF(screen, keep.bck, keep.pos.x, keep.pos.y);
    UpdateScreen();
    return;
  }
  MessageWindow(msgID);
}

/*---------------------------RemoveRumor--------------------------*/
void GraphicsEngine::RemoveRumor(void)
{
    GlobalData* data = m_pGame->GetData();
    Map* map = m_pGame->GetMap();
    /*Clear the rumor flag for the tile
      Replace the unit icon with stored background
      Redraw the display (SetTerrain)
      Update the stored background (bck1)
      Put the unit icon back on the screen*/

    map->getTile(data->mapnum)->setFlags(TILE_RUMOR, false);
    DrawSURF(screen,bck1,data->stile[data->oldloc][XPOS],
             data->stile[data->oldloc][YPOS]-data->yextra-5);
    SetTerrain();
    UpdateScreen(data->stile[data->scrnum][XPOS]-25,
                  data->stile[data->scrnum][YPOS]-25,
                  data->tw+50,data->th+50);
    DrawTILE(bck1,screen,0,0,data->tw,data->th,data->stile[data->scrnum][XPOS],
             data->stile[data->scrnum][YPOS]-data->yextra-5);
    UpdateScreen(data->stile[data->scrnum][XPOS],
                  data->stile[data->scrnum][YPOS]-data->yextra-5,
                  data->tw,data->th);
}

/*---------------------------DisplayMessage------------------------------*/
SDLMessage GraphicsEngine::DisplayMessage(string text,SDLFont *font,SDL_Surface* surf,
                                      SDL_Rect* pos){
		
	SDL_Rect destinationRect;
	SDL_Surface *text_surface,*messageSurface;
	SDLMessage mess;

	//The offset on the background image where to start the text
	int xOffset = 15;
	int yOffset = 15;

	//If not specified, default message background image
	if(surf == NULL)
		messageSurface  = messagenew;

	//If not specified, center the image on screen
	if(pos == NULL){
		//Search the center of the screen for the current message bgnd image
		destinationRect.x = ( screenResX / 2 ) - messageSurface->w /2;
		destinationRect.y = ( screenResY / 2 ) - messageSurface->h / 2;
	}
	//Otherwise, use specified destination
	else {
		destinationRect = *pos;
	}
	
	//Retreive space needed for the text according to font size
	int renderW, renderH, numberOfLinesNeeded = 0;
	char *cbegin = (char *)text.c_str(), *temp = cbegin;
	
	while(1)
	{
	 for(; *temp && (*temp != ' ') && (*temp != '\n'); temp++);
	 if(*temp == '\n') //added 26/6; possibility of using '\n' char to make a newline
	 {
        *temp = 0;
        TTF_SizeText(font->font, cbegin, &renderW, &renderH);
        *temp = '\n';
	 }
	 else if(*temp == ' ')
     {
        *temp = 0;
        TTF_SizeText(font->font, cbegin, &renderW, &renderH);
        *temp = ' ';
     }
     else
        TTF_SizeText(font->font, cbegin, &renderW, &renderH);

     if(*temp == '\n')
     {
        numberOfLinesNeeded++;
        temp++;
        cbegin = temp;
     }
     else if(renderW / (messageSurface->w-(2*xOffset)))
     {
		numberOfLinesNeeded++;
		char *cend = temp - 1;
		for(; cend != cbegin && *cend != ' ' && *cend != '\n'; cend--);
		if(cend == cbegin)
		 cend = temp;
	    if(!*cend) break;
	    temp = cbegin = cend + 1;
	 }
	 else if(!*temp)
	 {
      numberOfLinesNeeded++;
	  break;
     }
     else
     {
      temp++;
     }
	}
	#ifdef DEBUG
	cout << "NOLN: " << numberOfLinesNeeded<<endl;
	#endif

	//Starting point for text rendering
    if(((messageSurface->h - (numberOfLinesNeeded*renderH))/2)<=0)
    {
        #ifdef DEBUG
        cout<<"Message Window to small, resizing..."<<endl;
        #endif
        SDL_Surface *zoom;
        int w = messageSurface->w, h = ((numberOfLinesNeeded+1)*renderH) + yOffset;
        double dw = 1.0, dh = ((double) h) / ((double) messageSurface->h);
        #ifdef DEBUG
        cout<<"Zoom factor: "<<dw<<"x"<<dh<<endl;
        #endif
        zoom = zoomSurface(messageSurface, dw, dh, SMOOTHING_ON);
        if(zoom)
        {
            #ifdef DEBUG
            cout<<"Zoomed image created"<<endl;
            #endif
            #ifdef DEBUG
            cout<<"Resizing done at "<<w<<"x"<<h<<". Displaying the final image"<<endl;
            #endif
            messageSurface = CreateSurface(screen, w, h);
            mess.bck = CreateSurface(screen, w, h);
            SDL_BlitSurface(zoom, NULL, messageSurface, NULL);
            #ifdef DEBUG
            cout<<"Displaying Message Surface"<<endl;
            #endif
            if(pos==NULL)
            {
                destinationRect.x = ( screenResX / 2 ) - zoom->w / 2;
                destinationRect.y = ( screenResY / 2 ) - zoom->h / 2;
            }
            destinationRect.w = w;
            destinationRect.h = h;
            DrawTILE(mess.bck,screen,0,0,w,h, (screenResX - w) / 2,(screenResY - h) / 2);
            mess.pos = destinationRect;
            SDL_BlitSurface(zoom,NULL,screen,&destinationRect);
            SDL_FreeSurface(zoom);
            destinationRect.x += xOffset;
            destinationRect.y += yOffset;
        }
    }
	else
    {
        #ifdef DEBUG
        cout<<"Bliting the background image"<<endl;
        #endif
	    //Blit the background image, without text for now
	    int w = messageSurface->w, h = messageSurface->h;
	    mess.bck = CreateSurface(screen, w, h); //added 30/6; fixed crash when receiving a new unit
        DrawTILE(mess.bck,screen,0,0,w,h, (screenResX - w) / 2,(screenResY - h) / 2);
        mess.pos = destinationRect;
	    SDL_BlitSurface(messageSurface,NULL,screen,&destinationRect);
	
        if(numberOfLinesNeeded==1)
        {
	       destinationRect.x += (messageSurface->w - renderW) / 2;
        }
        else
        {
            destinationRect.x += xOffset;
        }
        destinationRect.y += (messageSurface->h - (numberOfLinesNeeded*renderH))/2;
    }

    temp = cbegin = (char *)text.c_str();
    
    #ifdef DEBUG
    cout<<"Displaying Message Window"<<endl;
    cout<<temp;
    #endif
	while(1)
	{
	 for(; *temp && *temp != ' ' && *temp != '\n'; temp++);
	 if(*temp == '\n')
	 {
	   *temp = 0;
       TTF_SizeText(font->font, cbegin, &renderW, &renderH);
       *temp = '\n';
	 
	 }
	 else if(*temp == ' ')
     {
        *temp = 0;
       TTF_SizeText(font->font, cbegin, &renderW, &renderH);
       *temp = ' ';
     }
     else
        TTF_SizeText(font->font, cbegin, &renderW, &renderH);

     if(*temp == '\n')
     {
        *temp = 0;
        text_surface = TTF_RenderText_Blended(font->font,cbegin,font->color);
		SDL_BlitSurface(text_surface,NULL,screen,&destinationRect);
		SDL_FreeSurface(text_surface);
   		destinationRect.y += renderH;
   		*temp = '\n';
   		temp++;
   		cbegin = temp;
     }
     else if(renderW / (messageSurface->w-(2*xOffset)))
     {
		char *cend = temp - 1;
		for(; cend != cbegin && *cend != ' ' && *cend != '\n'; cend--);
		if(cend == cbegin)
		 cend = temp;
		
        if(*cend == ' ' || *cend == '\n')
        { 
         char ck = *cend;
		 *cend = 0;
         text_surface = TTF_RenderText_Blended(font->font,cbegin,font->color);
		 SDL_BlitSurface(text_surface,NULL,screen,&destinationRect);
		 SDL_FreeSurface(text_surface);
   		 destinationRect.y += renderH;
		 *cend = ck;
	    }
	    else
	    {
         text_surface = TTF_RenderText_Blended(font->font,cbegin,font->color);
		 SDL_BlitSurface(text_surface,NULL,screen,&destinationRect);
		 SDL_FreeSurface(text_surface);
		 destinationRect.y += renderH;
	    }
		 
	    if(!*cend) break;
	    temp = cbegin = cend + 1;
	 }
	 else if(!*temp)
	 {
      text_surface = TTF_RenderText_Blended(font->font,cbegin,font->color);
      SDL_BlitSurface(text_surface,NULL,screen,&destinationRect);
      SDL_FreeSurface(text_surface);
      destinationRect.y += renderH;
	  break;
     }
     else
     {
      temp++;
     }
	}


	UpdateScreen();
	
	return mess;
}

// >>>

SDLMessage GraphicsEngine::ShowSysMessage(int m, Unit *att, Unit *def)
{
    GlobalData *data = m_pGame->GetData();
    string text = "Combat Statistics\n";
    char buffer[33];
    int buf;
		
    //added 26/6; Just showing some info details
    text += "Attacker: " + att->getName() + "\n";
    buf = att->getAttack();
		sprintf(buffer, "%d", buf);
		
    text += "          Power  : " + (string) buffer + "\n";
		buf = att->getDefense();
		sprintf(buffer, "%d", buf);
    text += "          Defence: " + (string) buffer + "\n\n";
    if(def)
    {
        text += "Defender: " + def->getName() + "\n";
        buf = def->getAttack();
    		sprintf(buffer, "%d", buf);
        text += "          Power  : " + (string) buffer + "\n";
        buf = def->getDefense();
    		sprintf(buffer, "%d", buf);
        text += "          Defence: " + (string) buffer + "\n\n";
    }
		
    text += data->msg->GetSysMessage(m);
    #ifdef DEBUG
    cout<<text<<endl;
    #endif
    return DisplayMessage(text,fontInformation);
}

SDLMessage GraphicsEngine::ShowSysMessage(int message, long skill)
{
  GlobalData *data = m_pGame->GetData();
  char *text = (char *)data->msg->GetSysMessage(message).c_str();
  string temp = "", mesg = "";
  int i, j, s;
  #ifdef DEBUG
  cout<<text<<endl;
  #endif
  for(i=0;i<strlen(text); i++)
  {
    #ifdef DEBUG
    cout<<text[i];
    #endif
    if(text[i]=='%')
    {
      temp = "";
      for(j=i+1;j<strlen(text) && text[j]!='%'; j++)
        temp += text[j];
      if(temp=="skill")
      {
        temp = "[Skill Not Found]";
        for(s=0; s<data->skillList.size(); s++)
        {
          if(data->skillList[s]->getType()==skill)
          {
            temp = data->skillList[s]->getName();
            break;
          }
        }
        #ifdef DEBUG
        cout<<temp;
        #endif
        mesg += temp;
      }
      i = j+1;
    }
    if(i<strlen(text)) mesg += text[i];
  }
  #ifdef DEBUG
  cout<<endl<<"message: "<<mesg<<endl;
  #endif
  return DisplayMessage(mesg, fontInformation);
}

SDLMessage GraphicsEngine::ShowSysMessage(int message)
{
    GlobalData* data = m_pGame->GetData();
    Map* map = m_pGame->GetMap();

    string text = data->msg->GetSysMessage(message);
    return DisplayMessage(text, fontInformation);
}

Message::Message()
{
    SysMsg = new vector<string>;
    VillMsg = new vector<string>;
    RumorMsg = new vector<string>;
    ReadMessages();
}

Message::~Message()
{
}

void Message::ParseXml(string xmlFile, vector<string> *array, string root, string item)
{
    string tmpString;
 
    TiXmlBase::SetCondenseWhiteSpace(true);
    TiXmlDocument labelFile(xmlFile.c_str());
    
    bool loadOkay = labelFile.LoadFile();

	if(!loadOkay)
	{
		cerr << "Could not load file '" << xmlFile << "'. Error= " <<
         labelFile.ErrorDesc() << ".\nExiting...\n";
		return;
	}
    
    TiXmlNode*	node			= 0;
    TiXmlElement* listElement	= 0;
	TiXmlElement* element		= 0;
	
	node = labelFile.FirstChild(root.c_str());
	listElement = node->ToElement();
	
	node = listElement->FirstChild(item.c_str());
	element = node->ToElement();
	
	do {
		node = element->FirstChild("text");
		tmpString = node->FirstChild()->Value();
		
		array->push_back(tmpString);
		
		element = element->NextSiblingElement();
	} while(element);
}

void Message::ReadMessages()
{
    ParseXml("data/text/datarumors.xml", RumorMsg, "rumorlist", "rumor");
    ParseXml("data/text/datavillage.xml", VillMsg, "villagemessagelist", "message");
    ParseXml("data/text/datasys.xml", SysMsg, "sysmessagelist", "message");
}

string Message::GetVillageMessage(Tile *village)
{
    int k = rand() % VillMsg->size();
    return (*VillMsg)[k];
}

string Message::GetRumorMessage(Tile *rumor)
{
    int k = rand() % RumorMsg->size();
    return (*RumorMsg)[k];
}

string Message::GetSysMessage(int message)
{
    string s;
    if(message >= SysMsg->size())
    {
         cerr << "Message::GetSysMessage: invalid message ID " << message << endl;
         return "Invalid message";
    }
    return (*SysMsg)[message];
}

char *Message::GetShortNation(int nation)
{
    switch(nation)
    {
        case NATION_ENG:
                return "Eng.";
        case NATION_FR:
                return "Fr.";
        case NATION_SPAN:
                return "Span.";
        case NATION_DUT:
                return "Dut.";
    }
    return "Err.";
}

char Message::GetOrderLetter(Uint8 order)
{
    switch(order)
    {
        case ORD_NONE:
                return '-';
        case ORD_CLEAR:
                return 'C';
        case ORD_PLOW:
                return 'P';
        case ORD_ROAD:
                return 'R';
        case ORD_FORTIFY: case ORD_FORTIFIED:
                return 'F';
        case ORD_SENTRY:
                return 'S';
    }
    return 'X';
}

//
// $Id$
//

