/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "graphicsEngine.h"
#include "map.h"

/*---------------------------Orders----------------------------*/
void GraphicsEngine::Orders(int orderID)  //in Unit class??
{
    GlobalData *data = m_pGame->GetData();
    Map *map = m_pGame->GetMap();

/*  Check to see if the tile to be plowed is already plowed,
    or if the tile to get a colony already has a colony, etc.
    See if the tile is a water, or Peak tile*/

    switch(orderID)
    {
        case ORD_CLEAR:
            if(map->getTile(data->mapnum)->Is(TILE_FOREST))
            {
              map->ClearForest(map->getTile(data->mapnum));
              ScreenUpdate();
              data->mf = 0;
              return;
            }
            //added 6/1
            //if no forest
            MessageWindow(MSG_SYSTEM, SYS_ALREADYCLEARED);
            return;;
        case ORD_PLOW:
            #ifdef DEBUG
            cout<<"Unit: "<<data->turn<<endl;
            #endif
            if(data->unitList[data->turn]->getType() !=IDU_PIONEER &&
               !data->unitList[data->turn]->getSkill())
            {
              #ifdef DEBUG
              cout<<"Plow field"<<endl;
              #endif
              MessageWindow(MSG_WRONGSKILL, (long)IDS_PIONEER);
              return;
            }
            if(data->unitList[data->turn]->getType() !=IDU_PIONEER &&
               data->unitList[data->turn]->getSkill()->getType()!=IDS_PIONEER)
            {
              #ifdef DEBUG
              cout<<"Plow field"<<endl;
              #endif
              MessageWindow(MSG_WRONGSKILL, (long)IDS_PIONEER);
              return;
            }
            if(!(map->getTile(data->mapnum)->Is(TILE_PLOWED) ||
                 map->getTile(data->mapnum)->Is(TILE_WATER)  ||
                 map->getTile(data->mapnum)->Is(TILE_PEAK)   ||
                 map->getTile(data->mapnum)->Is(TILE_FOREST)))
            {
              map->getTile(data->mapnum)->setFlags(TILE_PLOWED, true);
              ScreenUpdate();
              data->mf = 0;
              return;
            }
            //added 6/1
            //if already plowed
            if(map->getTile(data->mapnum)->Is(TILE_PLOWED))
            {
                MessageWindow(MSG_SYSTEM, SYS_ALREADYPLOWED);
            }
            return;;
        case ORD_ROAD:
            if(!(map->getTile(data->mapnum)->Is(TILE_ROAD)  ||
                 map->getTile(data->mapnum)->Is(TILE_WATER) ||
                 map->getTile(data->mapnum)->Is(TILE_PEAK)))
            {
                map->PlaceRoad(map->getTile(data->mapnum));
                ScreenUpdate();
                data->mf = 0;
            }
  //added 6/1
  //if already road        
            else if(map->getTile(data->mapnum)->Is(TILE_ROAD))
            {
                MessageWindow(MSG_SYSTEM, SYS_ALREADYROAD); 
            }
            break;
        case ORD_ENDTURN:
        {
            data->mf = 0;
            // to fix the 'E' key and visibility issue
            data->visible = 1;
            break;
        }
        case ORD_BUILD:
        {
            if(!(map->getTile(data->mapnum)->Is(TILE_BUILD) ||
                   map->getTile(data->mapnum)->Is(TILE_WATER) ||
                   map->getTile(data->mapnum)->Is(TILE_PEAK)))
            {
                Colony *col = m_pGame->PlaceColony(data->unitList[data->turn], data->mapnum);        
                ScreenUpdate();
                
                // if player, show CDS
                // TODO: replace this with a human/AI check
                if(data->unitList[data->turn]->getNation() == data->nation)
                {
                    DrawTILE(map1, screen, 0, 0, screenResX, screenResY, 0, 0);
                    DrawTILE(screen, cds1, 0, 0, screenResX, screenResY, 0, 0);
                    UpdateCDS(col);
                    UpdateScreen();
                    data->scrndsply = CDS;
                }
            }
      //added 6/1
      //if already a colony      
            else if(map->getTile(data->mapnum)->Is(TILE_BUILD))
            {
                MessageWindow(MSG_SYSTEM, SYS_ALREADYCOLONY); 
            }
            break;
        }
        default:
        {
            cerr << "Invalid order; check calls to GraphicsEngine::Orders";
            break;
        }
    }
}

//
// $Id$
//

