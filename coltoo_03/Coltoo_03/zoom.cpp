/*
 *  Copyright (C) 2003-2004 The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "graphicsEngine.h"
#include "map.h"
#include "Indians.h"

/*---------------------------Zoom1-------------------------------*/
void GraphicsEngine::ToggleZoom(void)
{
    GlobalData* data = m_pGame->GetData();

    if(!data->zl)
    {
        data->zl = 1;
        DrawTILE(map1,screen,0,0,836,708,0,20);
        DrawTILE(screen,mapback,0,20,836,708,0,0);
        //Border(screen,362,184,299,399);
        DisplayZoom(); //changed 1/7; reserver MiniMap to display the left window MiniMap
        // TODO: (Sikon) maybe more descriptive names than Zoom1 and Zoom2?
        // To Sikon (Chainsword): What about Zoom1=ToggleZoom and Zoom2=DisplayZoom?
        //Basically is what they do
    }
    else
    {
        data->zl = 0;
        DrawTILE(screen,map1,0,20,836,708,0,0);
        DrawTILE(screen,mapback,842,250,180,39,842,250);
    }
    UpdateScreen();   
}

/*---------------------------MiniMap------------------------------*/
void GraphicsEngine::DisplayZoom()
{
  DisplayZoom(SMOOTHING_OFF);
}

void GraphicsEngine::DisplayZoom(int smooth)
{
  //changed 08/07; Using minimap to draw the map and stretching it to the zoom window with
  //zoomSurface. Since the MiniMap is updated every turn there is no need to draw it again
  int dw = 525, dh = 700;   //zoom window size
  int x0=(836-dw)/2, y0=27; //zoom window position
  double dx = static_cast<double>(dw) / static_cast<double>(miniMapS->w);
  double dy = static_cast<double>(dh) / static_cast<double>(miniMapS->h);
    
  if(!miniMapS) MiniMap(false);
  if(!miniMapS) return;

  #ifdef DEBUG
  cout<<"Zooming ["<<dx<<"x"<<dy<<"]";
  if(smooth==SMOOTHING_ON) cout<<" smoothing=on";
  else cout<<" smoothing=off";
  cout<<endl;
  #endif
  DisplayZoomInfo();
  SDL_Surface *mmap = zoomSurface(miniMapS, dx, dy, smooth);
  if(mmap)
  {
    #ifdef DEBUG
    cout<<"Drawing zoomed map"<<endl;
    #endif
    DrawSURF(screen, mmap, x0, y0);
    Border(screen, x0, y0, dw, dh);
    SDL_FreeSurface(mmap);
  }
}

void GraphicsEngine::MiniMap()
{
  MiniMap(true);
}

void GraphicsEngine::MiniMap(bool display)
{
  GlobalData *data = m_pGame->GetData();
  Map *map = m_pGame->GetMap();
  int dw = 180, dh = 180, size = 1;
  SDL_Surface *mmap; // = CreateSurface(screen, dw, dh);
  SDL_Surface *temp = CreateSurface(screen, map->GetWidth() * size, map->GetHeight() * size);
  int x, y, i;
  double dx, dy;
  ColorData *c;
    
  if(!miniMapS) miniMapS = CreateSurface(screen, map->GetWidth() * size, map->GetHeight() * size);
  if(!miniMapS) return;
    
  dx = static_cast<double>(dw) / static_cast<double>(map->GetWidth() * size);
  dy = static_cast<double>(dh) / static_cast<double>(map->GetHeight() * size);
    
  i = 0;
  for(y = 0; y < map->GetHeight() * size; y += size)
  {
    for(x = 0; x < map->GetWidth() * size; x += size)
    {
      Tile *tile = map->getTile(i);
      if(tile->hasUnitsWalking() && tile->getShrdstate(data->nationTurn) != SHROUD_FULL)
      {
        //must be less than NATION_TRIBE; There are 4 tribes (from 0 to 3) each one
        //has nation = NATION_TRIBE + nativeID
        if(tile->getWalkingUnit()->getNation() < NATION_TRIBE)
        {
          if(tile->getWalkingUnit()->getUnitID() == data->turn)
          {
            c = new ColorData();
            c->SetRed(200);
            c->SetGreen(200);
            c->SetBlue(0);
          }
          else
          {
            c = new ColorData();
            c->SetRed(data->playerlist[tile->getWalkingUnit()->getNation()]->Color->Red());
            c->SetGreen(data->playerlist[tile->getWalkingUnit()->getNation()]->Color->Green());
            c->SetBlue(data->playerlist[tile->getWalkingUnit()->getNation()]->Color->Blue());
          }
        }
        else
        {
          c = new ColorData();
          c->SetRed(data->indians->getIndian(tile->getWalkingUnit()->getNation()-NATION_TRIBE)->Color->Red());
          c->SetGreen(data->indians->getIndian(tile->getWalkingUnit()->getNation()-NATION_TRIBE)->Color->Green());
          c->SetBlue(data->indians->getIndian(tile->getWalkingUnit()->getNation()-NATION_TRIBE)->Color->Blue());
        }
      }
      else c = TileColor(map->getTile(i));
      FillRect(temp, x, y, size, size, c->Red(), c->Green(), c->Blue());
      if(c) delete(c);
      i++;
    }
  }
  DrawSURF(miniMapS, temp, 0, 0);
  if(display)
  {
    x = data->start % map->GetWidth();
    y = data->start / map->GetWidth();
    DrawRect(temp, x, y, 13, 10, 255, 255, 0);
    
    mmap = zoomSurface(temp, dx, dy, SMOOTHING_ON);
    if(mmap)
    {
      DrawSURF(screen, mmap, 842, 50);
      Border(screen, 842, 50, dw, dh);
      SDL_FreeSurface(mmap);
    }
  }
  SDL_FreeSurface(temp);
}

ColorData *GraphicsEngine::TileColor(int num)
{
  Map *map = m_pGame->GetMap();
  return TileColor(map->getTile(num));
}

ColorData *GraphicsEngine::TileColor(Tile *tile)
{
  ColorData *c = new ColorData();
  if(!tile) { c->SetColor("0, 0, 0"); return c; }
  GlobalData *data = m_pGame->GetData();
  int k = tile->getTerrain();
  int s = tile->getShrdstate(data->nationTurn);
  switch (k)
  {
    case (TERRAIN_OCEAN):
    c->SetRed(0); c->SetGreen(64); c->SetBlue(190);
    if(s==1) { c->SetRed(0); c->SetGreen(32); c->SetBlue(95); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_HIGHSEAS):                    //added 5/23
    c->SetRed(16); c->SetGreen(96); c->SetBlue(160);
    if(s==1) { c->SetRed(8); c->SetGreen(64); c->SetBlue(95); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_ARCTIC):
    c->SetRed(255); c->SetGreen(255); c->SetBlue(255);
    if(tile->Is(TILE_WATER) && tile->Is(TILE_RIVER)) { c->SetRed(192); c->SetGreen(192); c->SetBlue(255); }
    if(s==1) { c->SetRed(128); c->SetGreen(128); c->SetBlue(128); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_TUNDRA):
    c->SetRed(96); c->SetGreen(160); c->SetBlue(176);
    if(tile->Is(TILE_FOREST)) { c->SetRed(64); c->SetGreen(128); c->SetBlue(144); }
    if(s==1) { c->SetRed(48); c->SetGreen(80); c->SetBlue(88); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_PRAIRIE):
    c->SetRed(160); c->SetGreen(160); c->SetBlue(80);
    if(tile->Is(TILE_FOREST)) { c->SetRed(120); c->SetGreen(120); c->SetBlue(60); }
    if(s==1) { c->SetRed(80); c->SetGreen(80); c->SetBlue(40); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_GRASS):
    c->SetRed(96); c->SetGreen(196); c->SetBlue(32);
    if(tile->Is(TILE_FOREST)) { c->SetRed(64); c->SetGreen(128); c->SetBlue(24); }
    if(s==1) { c->SetRed(48); c->SetGreen(98); c->SetBlue(16); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_SAVANNAH):
    c->SetRed(96); c->SetGreen(192); c->SetBlue(128);
    if(tile->Is(TILE_FOREST)) { c->SetRed(64); c->SetGreen(160); c->SetBlue(96); }
    if(tile->Is(TILE_FOREST) && tile->Is(TILE_RAIN)) { c->SetRed(64); c->SetGreen(160); c->SetBlue(128); }
    if(s==1) { c->SetRed(0); c->SetGreen(32); c->SetBlue(95); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_DESERT):
    c->SetRed(180); c->SetGreen(160); c->SetBlue(104);
    if(tile->Is(TILE_FOREST)) { c->SetRed(120); c->SetGreen(130); c->SetBlue(60); }
    if(s==1) { c->SetRed(90); c->SetGreen(80); c->SetBlue(52); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_STEPPE):
    c->SetRed(80); c->SetGreen(150); c->SetBlue(110);
    if(s==1) { c->SetRed(40); c->SetGreen(75); c->SetBlue(55); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_OPENS):
    c->SetRed(120); c->SetGreen(140); c->SetBlue(100);
    if(tile->Is(TILE_FOREST)) { c->SetRed(100); c->SetGreen(120); c->SetBlue(80); }
    if(s==1) { c->SetRed(60); c->SetGreen(70); c->SetBlue(50); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    case (TERRAIN_CHAPARRAL):
    c->SetRed(120); c->SetGreen(100); c->SetBlue(60);
    if(s==1) { c->SetRed(60); c->SetGreen(50); c->SetBlue(30); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
    break;
    default:
    break;
  }

  if(tile->Is(TILE_RIVER))
  {
    c->SetRed(64); c->SetGreen(128); c->SetBlue(196);
    if(s==1) { c->SetRed(32); c->SetGreen(64); c->SetBlue(98); }
    if(s==2) { c->SetRed(0); c->SetGreen(0); c->SetBlue(0); }
  }
  return c;
}

void GraphicsEngine::DisplayZoomInfo()
{
  GlobalData* data = m_pGame->GetData();
  SDL_Surface *info = CreateSurface(screen, 180, 39);
  int x = 0, y = 0;
  bool initInfo = false;
  char text[33];
  SDL_Rect rect;
  
  if(!info) return;
  
  FillRect(info, 0, 0, 180, 39, 48, 48, 48);

  sprintf(text, "Zoom Window");
  DrawZoomInfoString(info, data->SoLfont1[3], x, y, text);
  sprintf(text,"'A' - Smoothing on");
  DrawZoomInfoString(info, data->SoLfont1[3], x, y, text);
  sprintf(text,"'S' - Smoothing off");
  DrawZoomInfoString(info, data->SoLfont1[3], x, y, text);

  rect.x = 842;
  rect.y = 250;
  SDL_BlitSurface(info,NULL,screen,&rect);
  Border(screen, 842, 250, 180, 39);
  SDL_FreeSurface(info);
}

void GraphicsEngine::DrawZoomInfoString(SDL_Surface *info, SDL_Surface *font, int x , int &y, const char *text)
{
  int xOffset = 5, yOffset = 5;
  drawString2(info, font, x + xOffset, y + yOffset, "%s", text);
  y += font->h;
}

//
// $Id$
//

