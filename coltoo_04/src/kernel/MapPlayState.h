/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Gamestate for when player is on main world map
*/

#ifndef MAPPLAYSTATE_H
#define MAPPLAYSTATE_H

#include "SDL/SDL.h"

#include "coltoo/Application.h"
#include "Playstate.h"

class MapPlayState : public PlayState
{
	public:
		MapPlayState();
		~MapPlayState();
		
		void init();
		void pause();
		void resume();
		void end();
		
		void input();
		void update();
		void draw();
		
		string identify();

	private:
	    void handleKey(int key);
};

#endif // MAPPLAYSTATE_H
