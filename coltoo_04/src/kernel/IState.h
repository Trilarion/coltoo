/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Gamestate interface definition
*/

#ifndef ISTATE_H
#define ISTATE_H

#include <iostream>
#include <string>
using std::string;

#include "memMgr/ManagedObject.h"
#include "memMgr/ManagedPointer.h"
#include "gfxEngine/GraphicsEngine.h"
#include "universe/Universe.h"

class IState : public ManagedObject
{
	public:
		IState();
		virtual ~IState();
		
		virtual void init()   = 0;
		virtual void pause()  = 0;
		virtual void resume() = 0;
        virtual void end()    = 0;
		
		virtual void input()  = 0;
		virtual void update() = 0;
		virtual void draw()   = 0;
		
		virtual string identify() = 0; //For debugging

		AUTO_SIZE;
};

typedef ManagedPointer<IState> MPState;

#include "StateMgr.h"

#endif // ISTATE_H

