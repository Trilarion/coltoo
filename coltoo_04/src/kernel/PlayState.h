/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Base class for playing state
*/

#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include "actors/Actor.h"

#include "IState.h" 

class PlayState : public IState
{
	public:
		PlayState();
		virtual ~PlayState();
		
		virtual void init()   = 0;
		virtual void pause()  = 0;
		virtual void resume() = 0;
        virtual void end()    = 0;
		
		virtual void input()  = 0;
		virtual void update() = 0;
		virtual void draw()   = 0;	
};

#endif // PLAYSTATE_H
