/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Keeps track of game states
*/

#ifndef STATEMGR_H
#define STATEMGR_H

#include <iostream>

#include <stack>
using std::stack;

#include "IState.h"
#include "memMgr/Singleton.h"
#include "memMgr/ManagedPointer.h"

class StateMgr : public Singleton<StateMgr>
{
	public:
		StateMgr();
		~StateMgr();
		
		void changeState(MPState aState); //Replace current state
		void pushState(MPState aState);   //Suspend current state and start other
		void popState();                  //End current state (and resume previous)
		
		void popAll();                    //Emergency clean up
		
		//For the game loop
		void input(); 
		void update();
		void draw();
  		
  		//For debugging
		unsigned long countStates() const;   
        void printStack();   
		
	private:
	    stack< ManagedPointer<IState> > gameStates;
};

#endif // STATEMGR_H
