/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "MapPlayState.h"

MapPlayState::MapPlayState()
{
}

MapPlayState::~MapPlayState()
{
}

void MapPlayState::init()
{
    GraphicsEngine::get().prepMapScreen();
}

void MapPlayState::pause()
{
}

void MapPlayState::resume()
{
}

void MapPlayState::end()
{
}

void MapPlayState::input()
{
    //This will probably move when we introduce AI
    SDL_Event event;
    
    /* TODO (#1#): work on CPU usage
                   PollEvent uses 100% CPU
                   WaitEvent doesn't, but it's not catching the events */   
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_KEYDOWN:
                handleKey(event.key.keysym.sym);
                break;
            case SDL_QUIT:
                Application::get().quit();
                break;
        }
    }    
}

void MapPlayState::update()
{
}

void MapPlayState::draw()
{
}

string MapPlayState::identify()
{
    return "MapPlay";
}

void MapPlayState::handleKey(int key)
{
    switch (key)
    {
        case SDLK_ESCAPE:
            Application::get().quit();
            break;                
    }
}
