/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Game application.  Handles proper execution of game states.
*/

#ifndef APPLICATION_H
#define APPLICATION_H

#include <iostream>

#include "memMgr/Singleton.h"
#include "kernel/StateMgr.h"
#include "kernel/IState.h"
#include "kernel/MainMenuState.h"
#include "kernel/PlayState.h"

class Application : public Singleton<Application>
{
	public:
		Application();
		~Application();
		
		void run();
		void quit();
		
		bool quitApp;
	
	private:
	    StateMgr* stateMgr;
};

#endif // APPLICATION_H

