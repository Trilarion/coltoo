/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "Map.h"

Map::Map() : width(0), height(0), area(0)
{
}

Map::Map(string fileName)
{
    std::cout << "Making map." << std::endl;

    ifstream inFile(fileName.c_str());
    if (!inFile)
        throw("Error loading map");

    /* TODO (#1#): Get rid of magic numbers */
    width  = 150;
    height = 200;
    area   = width*height;

    //Immediately reserve enough space to prevent resizing later
    tiles.reserve(area);
           
    //Assuming map in file doesn't exceed set width & height
    //We should include this information in the file 
    char c;
    while(inFile.get(c))
    {
        tiles.push_back(new Tile(c));
    }
    
    std::cout << "Made map with " << tiles.size() << " tiles." << std::endl;
}

Map::~Map()
{
    std::cout << "Deleting map" << std::endl;
    
    tiles.erase(tiles.begin(), tiles.end());
    
    std::cout << "Deleted map" << std::endl;
}

Tile* Map::findTile(coord_t x, coord_t y)
{
    if (x > width || y > height)
        return 0;
    
    return tiles.at(x + (y-1)*width - 1);
}
