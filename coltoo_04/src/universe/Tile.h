/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Deifnes a map tile
*/

#ifndef TILE_H
#define TILE_H

#include <bitset>
using std::bitset;

#include "memMgr/ManagedObject.h"
#include "memMgr/ManagedPointer.h"

/* TODO (#1#): Check if terrainTypes and tileTypes are OK.
               Some elements seem to be in the wrong enum */
enum terrainTypes { TERRAIN_ARCTIC    = 1  , TERRAIN_TUNDRA = 77 ,
                    TERRAIN_DESERT    = 153, TERRAIN_OPENS  = 229,
                    TERRAIN_SAVANNAH  = 305, TERRAIN_GRASS  = 381,
                    TERRAIN_PRAIRIE   = 457, TERRAIN_STEPPE = 533,
                    TERRAIN_CHAPARRAL = 609, TERRAIN_LAKE   = 685,
                    TERRAIN_OCEAN     = 761, TERRAIN_RUMOR  = 381
                  };

enum tileFlags { TILE_FOREST = 0  , TILE_RAIN    , TILE_MOUNTAIN  , 
                 TILE_HILL        , TILE_PEAK     , TILE_RIVER     ,     
                 TILE_WATER       , TILE_CLEAR    , TILE_PLOWED    , 
                 TILE_ROAD        , TILE_BUILD    , TILE_FAKE      ,
                 TILE_RUMOR       , TILE_FAKERUMOR, TILE_FAKEFOREST, 
                 TILE_FAKEMOUNTAIN, TILE_FAKEHILL , TILE_FAKEPEAK  ,
                 TILE_UNIT 
               };

//Not using ManagedObject for performance reasons
class Tile //: public ManagedObject
{
    public:
        Tile();
        Tile(char aTileCode);
        ~Tile();

        void setFlag(enum tileFlags f, bool onOff);        
        void setTerrain(enum terrainTypes t);
        
        //AUTO_SIZE;
      
    private:
        char tileCode;        
        terrainTypes terrain;
        bitset<18> flags; //See enum tileFlags above for positions
};

//typedef ManagedPointer<Tile> MPTile;

#endif //TILE_H
