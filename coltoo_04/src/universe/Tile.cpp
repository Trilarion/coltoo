/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "Tile.h"

Tile::Tile()
{
    flags.reset();
}

Tile::Tile(char aTileCode)
{
    tileCode = aTileCode;
    switch(tileCode)
    {
        case 'A': setFlag(TILE_RIVER, true);
        case 'a': setTerrain(TERRAIN_OCEAN);
                  setFlag(TILE_WATER, true);
                  break;

        case 'b': setTerrain(TERRAIN_LAKE);
                  setFlag(TILE_WATER, true);
                  break;

        case 'C': setFlag(TILE_RIVER, true);
        case 'c': setTerrain(TERRAIN_ARCTIC);
                  break;

        case 'D': setFlag(TILE_RIVER, true);
        case 'd': setTerrain(TERRAIN_TUNDRA);
                  break;

        case 'E': setFlag(TILE_RIVER, true);
        case 'e': setTerrain(TERRAIN_PRAIRIE);
                  break;

        case 'F': setFlag(TILE_RIVER, true);
        case 'f': setTerrain(TERRAIN_GRASS);
                  break;

        case 'G': setFlag(TILE_RIVER,true);
        case 'g': setTerrain(TERRAIN_SAVANNAH);
                  break;

        case 'H': setFlag(TILE_RIVER, true);
        case 'h': setTerrain(TERRAIN_DESERT);
                  break;

        case 'I': setFlag(TILE_RIVER, true);
        case 'i': setTerrain(TERRAIN_TUNDRA); /*boreal*/
                  setFlag(TILE_FOREST, true);
                  break;

        case 'J': setFlag(TILE_RIVER, true);
        case 'j': setTerrain(TERRAIN_GRASS);  /*conifer*/
                  setFlag(TILE_FOREST, true);
                  break;

        case 'K': setFlag(TILE_RIVER, true);
        case 'k': setTerrain(TERRAIN_OPENS); /*mixed*/
                  setFlag(TILE_FOREST, true);
                  break;

        case 'L': setFlag(TILE_RIVER, true);
        case 'l': setTerrain(TERRAIN_PRAIRIE); /*broad*/
                  setFlag(TILE_FOREST, true);
                  break;

        case 'M': setFlag(TILE_RIVER, true);
        case 'm': setTerrain(TERRAIN_SAVANNAH); /*tropic*/
                  setFlag(TILE_FOREST, true);
                  break;

        case 'N': setFlag(TILE_RAIN, true);
        case 'n': setTerrain(TERRAIN_SAVANNAH); /*rain*/
                  setFlag(TILE_FOREST, true);
                  setFlag(TILE_RIVER, true);
                  break;

        case 'O': setFlag(TILE_RIVER, true);
        case 'o': setTerrain(TERRAIN_DESERT);  /*shrub*/
                  setFlag(TILE_FOREST, true);
                  break;

        case 'Q': setFlag(TILE_RIVER, true);
        case 'q': setTerrain(TERRAIN_STEPPE);
                  break;

        case 'R': setFlag(TILE_RIVER, true);
        case 'r': setTerrain(TERRAIN_CHAPARRAL);
                  break;

        case 'S': setFlag(TILE_RIVER, true);
        case 's': setTerrain(TERRAIN_OPENS);
                  break;

        case 'T': setFlag(TILE_RIVER, true);
        case 't': setTerrain(TERRAIN_OPENS);   /*hill*/
                  setFlag(TILE_HILL, true);
                  break;

        case 'U': setFlag(TILE_RIVER, true);
        case 'u': setTerrain(TERRAIN_TUNDRA);  /*mountain*/
                  setFlag(TILE_MOUNTAIN, true);
                  break;

        case 'V': setFlag(TILE_RIVER, true);
        case 'v': setTerrain(TERRAIN_ARCTIC);  /*mountain peak*/
                  setFlag(TILE_PEAK, true);
                  break;

        default:  //setTerrain(TERRAIN_OPENS+37);
                  break;        
    }
}

Tile::~Tile()
{
}

void Tile::setFlag(enum tileFlags f, bool onOff)
{
    flags.set(f, onOff);
    //Special actions for forest & road?
}

void Tile::setTerrain(enum terrainTypes t)
{
    terrain = t;
}
