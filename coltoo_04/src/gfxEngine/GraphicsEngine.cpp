/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "GraphicsEngine.h"

GraphicsEngine::GraphicsEngine()
{
    init();
}

GraphicsEngine::~GraphicsEngine()
{	
    end();
}

void GraphicsEngine::init()
{
    /* TODO (#1#): Decide how to handle errors (throw our own errors or 
                   return boolean value) */
    
    //Fire up SDL
    if (SDL_Init(SDL_INIT_AUDIO|SDL_INIT_VIDEO)<0)
    {
        std::cerr << "Unable to init SDL; " << SDL_GetError() << std::endl;
        exit(1);
    }

    //Set resolution
    screenResX = 1024; screenResY = 768; 
	screen = SDL_SetVideoMode(screenResX, screenResY, 
                              0, SDL_SWSURFACE|SDL_HWPALETTE);
    if(!screen)
    {
        std::cerr << "Unable to set " << screenResX << "x" << screenResY
                  << " video:\n" << SDL_GetError() << std::endl;
        exit(1);
    }

    //Misc. window stuff
    /* TODO (#1#): Remove hardcoded strings */
    SDL_WM_SetCaption("Coltoo", "Coltoo");
    SDL_WM_SetIcon(SDL_LoadBMP("data/graphics/col2ico.bmp"), 0);

    //Font stuff
	fontPushButtons = new SDLFont();
	fontInformation = new SDLFont();

	if(TTF_Init()==-1) 
    {
		std::cerr << "TTF_Init: " << TTF_GetError() << std::endl;
		exit(1);
	}

	fontPushButtons->font = TTF_OpenFont("data/fonts/Vera.ttf", 12);
	fontInformation->font = TTF_OpenFont("data/fonts/Vera.ttf", 20);

	if(!fontPushButtons || !fontInformation)
	{
		std::cerr << "TTF_OpenFont: " << TTF_GetError() << std::endl;
		exit(1);
	}

	SDL_Color yellow = { 0xFF, 0xFF, 0x00, 0 };
	SDL_Color black  = { 0x00, 0x00, 0x00, 0 };
	fontPushButtons->color = yellow;
	fontInformation->color = black;
}

void GraphicsEngine::end()
{
    SDL_FreeSurface(screen);
    delete screen;
    
    SDL_FreeSurface(mapScreen);
    delete mapScreen;

	if(fontPushButtons)
    {
        if(fontPushButtons->font) 
            TTF_CloseFont(fontPushButtons->font);
        delete fontPushButtons;
    }
    if(fontInformation)
    {
        if(fontInformation->font) 
            TTF_CloseFont(fontInformation->font);
        delete fontInformation;
    }

    TTF_Quit();	    
	SDL_Quit();
}

void GraphicsEngine::splash()
{
    /* TODO (#1#): Get rid of hardcoded values */
    drawSurface(screen
               ,loadImage("data/graphics/coltoo2.jpg")
               ,227, 295
               );
    updateScreen();
    SDL_Delay(2000);
}

SDL_Surface* GraphicsEngine::loadImage(const string filename) const
{
    return SDL_DisplayFormat( IMG_Load(filename.c_str()) );
}
 
void GraphicsEngine::drawSurface(SDL_Surface* to, SDL_Surface* from, int x, int y)
{
    SDL_Rect dest;
    dest.x = x;
    dest.y = y;
    
    SDL_SetColorKey(from
                   ,SDL_SRCCOLORKEY
                   /* TODO (#1#): Remove hard coded RGB values?*/
                   ,SDL_MapRGB(from->format, 0, 0, 0));
                   
    SDL_BlitSurface(from, 0, to, &dest);
}

void GraphicsEngine::drawRect(SDL_Surface* to,SDL_Surface* from,int x,int y,
                              int w,int h,int x2,int y2)
{
    SDL_Rect dest;
    dest.x=x;
    dest.y=y;

    SDL_Rect dest2;
    dest2.x=x2;
    dest2.y=y2;
    dest2.w=w;
    dest2.h=h;
    SDL_SetColorKey(from
                   ,SDL_SRCCOLORKEY
                   ,SDL_MapRGB(from->format, 0, 0, 0));
    
    SDL_BlitSurface(from, &dest2, to, &dest);
}

void GraphicsEngine::drawString(SDL_Surface *surf, SDLFont *font,int x,int y,bool shadow,
                                const string str)
{
	if(shadow)
    {
		SDL_Color black = { 0x00, 0x00, 0x00, 0 };
		blitString(surf,font->font,x+1,y+1, str,black);
	}
	blitString(surf,font->font,x,y,str,font->color);
}

void GraphicsEngine::blitString(SDL_Surface *surf,TTF_Font *font,int x,int y,
								const string str,SDL_Color color)
{
	SDL_Surface *text_surface;
	SDL_Rect destinationRect;

    destinationRect.x=x;
    destinationRect.y=y;

	text_surface = TTF_RenderText_Blended(font,str.c_str(),color);

	SDL_BlitSurface(text_surface,NULL,surf,&destinationRect);

	SDL_FreeSurface(text_surface);
}

void GraphicsEngine::updateScreen()
{
/*hides the cursor; updates the entire screen; 
  shows the cursor again.*/
  
  //Why hide the cursor?
   
    SDL_ShowCursor(0);
    SDL_Flip(screen);
    SDL_ShowCursor(1);
}

void GraphicsEngine::prepMapScreen()
{
    // Init
 	mapScreen = SDL_CreateRGBSurface(SDL_SWSURFACE
                                    ,screenResX, screenResY
                                    ,screen->format->BitsPerPixel
                                    ,screen->format->Rmask
                                    ,screen->format->Gmask
                                    ,screen->format->Bmask
                                    ,screen->format->Amask);
    if(!mapScreen)
    {
        std::cerr << "Error creating map screen" << std::endl;
        exit(1);
    }                                                                        

    //Background
    SDL_Surface* tile = loadImage("data/graphics/bckgrnd.jpg");
    if (!tile)
    {
        std::cerr << "Error loading background" << std::endl;
        exit(1);
    }
    
    int tileSize = 256; //Length of side if background tile in pixels
    for(int y=0; y<screenResY; y+=tileSize)
    {
        for(int x=0; x<screenResX; x+=tileSize)
        {
            drawSurface(mapScreen, tile, x, y);
        }
    }
    
    SDL_FreeSurface(tile);  

    /* TODO (#1#): Remove string constants */
    /* TODO (#1#): Clean up */

    //Common stuff for buttons    
    tile = loadImage("data/graphics/tsheetxtra1.png");
    if (!tile)
    {
        std::cerr << "Error loading button images" << std::endl;
        exit(1);
    }

    // Menu buttons at top of screen
    drawRect(mapScreen, tile, 5, 2, 76, 18, 267, 52);
    drawString(mapScreen, fontPushButtons, 24,  2, true, "Game");
    drawRect(mapScreen, tile, 81, 2, 76, 18, 267, 52);
    drawString(mapScreen, fontPushButtons, 106, 2, true, "View");
    drawRect(mapScreen, tile, 157, 2, 76, 18, 267, 52);
    drawString(mapScreen, fontPushButtons, 176, 2, true, "Orders");
    drawRect(mapScreen, tile, 233, 2, 76, 18, 267, 52);
    drawString(mapScreen, fontPushButtons, 248, 2, true, "Reports");
    drawRect(mapScreen, tile, 309, 2, 76, 18, 267, 52);
    drawString(mapScreen, fontPushButtons, 330, 2, true, "Trade");
    drawRect(mapScreen, tile, 385, 2, 76, 18, 267, 52);
    drawString(mapScreen, fontPushButtons, 395, 2, true, "Zoom out");
    drawRect(mapScreen, tile, 461, 2, 132, 18, 1, 52);
    drawString(mapScreen, fontPushButtons, 494, 2, true, "Assistance");
    drawRect(mapScreen, tile, 943, 2, 76, 18, 267, 52);
    drawString(mapScreen, fontPushButtons, 962, 2, true, "Quit");

    //Buttons at side of screen
    drawRect(mapScreen, tile, 990, 50, 24, 24, 1, 71);
    drawString(mapScreen, fontPushButtons, 997, 54, true, "C");
    drawRect(mapScreen, tile, 990, 74, 24, 24, 1, 71);
    drawString(mapScreen, fontPushButtons, 997, 78, true, "P");
    drawRect(mapScreen, tile, 990, 98, 24, 24, 1, 71);
    drawString(mapScreen, fontPushButtons, 997, 102, true, "R");
    drawRect(mapScreen, tile, 990, 122, 24, 24, 1, 71);
    drawString(mapScreen, fontPushButtons, 997, 126, true, "B");
    drawRect(mapScreen, tile, 990, 146, 24, 24, 1, 71);
    drawString(mapScreen, fontPushButtons, 997, 150, true, "G");
    drawRect(mapScreen, tile, 990, 170, 24, 24, 1, 71);
    drawString(mapScreen, fontPushButtons, 997, 174, true, "E");

    //Buttons at bottom
    drawRect(mapScreen, tile, 438, 734, 49, 24, 51, 71);
    drawString(mapScreen, fontPushButtons, 450, 738, true, "CDS");
    drawRect(mapScreen, tile, 487, 734, 49, 24, 51, 71);
    drawString(mapScreen, fontPushButtons, 499, 738, true, "HCS");
    drawRect(mapScreen, tile, 536, 734, 49, 24, 51, 71);
    drawString(mapScreen, fontPushButtons, 548, 738, true, "MAP");
 
    SDL_FreeSurface(tile); 
        
    //Draw map

    //Show it
    drawSurface(screen, mapScreen, 0, 0);
    updateScreen();    
}
