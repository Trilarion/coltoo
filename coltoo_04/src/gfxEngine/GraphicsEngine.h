/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef graphicsEngine_H
#define graphicsEngine_H

#include <iostream>
#include <string>
using std::string;

#include "SDL/SDL.h"
#include "SDL/SDL_Image.h"
#include "SDL/SDL_ttf.h"
 
#include "memMgr/Singleton.h"

/* TODO (#1#): Move fonts to fontEngine? */
struct SDLFont
{
    TTF_Font	*font;
    SDL_Color	color;
};

class GraphicsEngine : public Singleton<GraphicsEngine>
{
    public:
        GraphicsEngine();
        ~GraphicsEngine();

        void init();
        void end();

        void splash();  //Show splash screen
        
        void prepMapScreen(); 
                
    private:
        unsigned int screenResX, screenResY;
        SDL_Surface* screen;
        SDL_Surface* mapScreen;
        
        SDLFont* fontPushButtons;
        SDLFont* fontInformation; 

        SDL_Surface* loadImage(const string filename) const;
        void drawSurface(SDL_Surface* to, SDL_Surface* from, int x, int y);                       
        void drawRect(SDL_Surface* to,SDL_Surface* from,int x,int y,
                              int w,int h,int x2,int y2);
        void drawString(SDL_Surface *screen,SDLFont *font,int x,int y,bool shadow,
                     const string str);
        void blitString(SDL_Surface *surf,TTF_Font *font,int x,int y,
								const string str,SDL_Color color);
        void updateScreen();
};

#endif //graphicsEngine_H
