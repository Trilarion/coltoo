/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Singleton base class.
    
    Based on example from Scott Bilas.
    http://www.drizzle.com/~scottb/publish/gpgems1_singleton.htm
    
    Initialised on first use.
*/

#ifndef SINGLETON_H
#define SINGLETON_H

template<class T>
class Singleton
{
	public:
	    static T& get();
	    static T* getPtr();
 
    protected:
		Singleton();
		virtual ~Singleton();
		
	private:
		static T* pinstance;
};

template<class T>
T* Singleton<T>::pinstance = 0;

template<class T>
T& Singleton<T>::get()
{
    if(!pinstance)
        pinstance = new T();
        
    return *pinstance;
}

template<class T>
T* Singleton<T>::getPtr()
{
    if (!pinstance)
        pinstance = new T();
        
    return pinstance;
}

template<class T>
Singleton<T>::Singleton()
{
    pinstance = static_cast<T*>(this);
}

template<class T>
Singleton<T>::~Singleton()
{
    pinstance = 0;
}

#endif // SINGLETON_H
