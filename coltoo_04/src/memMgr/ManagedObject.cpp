/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "ManagedObject.h" 

list<ManagedObject*> ManagedObject::usedObj;
list<ManagedObject*> ManagedObject::obsoleteObj;

ManagedObject::ManagedObject()
{
	usedObj.push_back(this);
	refCount = 0;
}


ManagedObject::~ManagedObject()
{
}

void ManagedObject::newRef()
{
    refCount++;
}

void ManagedObject::delRef()
{
    refCount--;
    //refCount < 0 shouldn't actually occur...
    if (refCount <= 0)
    {
        usedObj.remove(this);
        obsoleteObj.push_back(this);
    }
}

void ManagedObject::cleanObsolete()
{
    for (list<ManagedObject*>::iterator iter = obsoleteObj.begin()
        ;iter != obsoleteObj.end()
        ;iter++
        )
    {
        delete *iter;
    }
    
    obsoleteObj.clear();
}

void ManagedObject::cleanupAll()
{
    //This method should actually find anything to clean.
    //If it does, it means not all objects are properly cleaned up.
    //That would be a memory leak without this function.
    
    //First do the regular cleaning
    cleanObsolete();
    
    //And now mop up the stuff that went wrong
    /* TODO (#1#): Provide better error handling/logging */
    for (std::list<ManagedObject*>::iterator iter = usedObj.begin()
        ;iter != usedObj.end()
        ;++iter
        )
    {        
        std::cout << "Lingering object!" << std::endl;
        delete *iter;
    }
    
    usedObj.clear();
}

long ManagedObject::getRefCount() const
{
    return refCount;
}

long ManagedObject::usedObjCount()
{
    return usedObj.size();
}

long ManagedObject::obsoleteObjCount()
{
    return obsoleteObj.size();
}

unsigned long ManagedObject::usedObjSize()
{
    unsigned long sum = 0;
    
    for (std::list<ManagedObject*>::iterator iter = usedObj.begin()
        ;iter != usedObj.end()
        ;++iter
        )
    {
        sum += (*iter)->size();
    }
    return sum;
}

unsigned long ManagedObject::obsoleteObjSize()
{
    unsigned long sum = 0;
    
    for (std::list<ManagedObject*>::iterator iter = obsoleteObj.begin()
        ;iter != obsoleteObj.end()
        ;++iter
        )
    {
        sum += (*iter)->size();
    }
    return sum;
}

void ManagedObject::objStats()
{ 
    std::cout << "Objects: " << ManagedObject::usedObjCount() 
              << " / "       << ManagedObject::usedObjSize() 
              << " // "      << ManagedObject::obsoleteObjCount() 
              << " / "       << ManagedObject::obsoleteObjSize()
              << std::endl;
}
