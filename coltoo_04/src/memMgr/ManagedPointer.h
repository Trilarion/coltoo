/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Basic smart pointer for use with the garbage collector.  
*/

#ifndef MANAGEDPOINTER_H
#define MANAGEDPOINTER_H

template<class T> 
class ManagedPointer
{
	public:
		ManagedPointer();       
		ManagedPointer(T* aobj);		
		ManagedPointer(const ManagedPointer<T> &aptr);

		~ManagedPointer();
	
	    ManagedPointer<T>& operator =(T* aobj);
	    ManagedPointer<T>& operator =(ManagedPointer<T> &aptr);
	           
	    T& operator *  () const;
	    T* operator -> () const;
	       operator T* () const; //Convert back to normal pointer
	    
  	    bool operator ==(const ManagedPointer<T> &aptr) const;
  	    bool operator !=(const ManagedPointer<T> &aptr) const;
  	    
  	    //For debugging
  	    long refCount() const;
                	    
	protected:
        T* obj;
};

template<class T>
ManagedPointer<T>::ManagedPointer()
{
    obj = 0;
}

template<class T>
ManagedPointer<T>::ManagedPointer(T* aobj)
{
    obj = 0;
    *this = aobj;
}
		
template<class T>
ManagedPointer<T>::ManagedPointer(const ManagedPointer<T> &aptr)
{
    obj = 0;
    *this = aptr;
}

template<class T>	
ManagedPointer<T>::~ManagedPointer()
{
    if (obj)
        obj->delRef();		
}

template<class T>	
inline ManagedPointer<T>& ManagedPointer<T>::operator =(T* aobj)
{
    if (obj)
        obj->delRef();
        
    obj = aobj;
    
    if (obj)
        obj->newRef();
        
    return *this;
}

template<class T>   	
inline ManagedPointer<T>& ManagedPointer<T>::operator =(ManagedPointer<T> &aptr)
{
    if (obj)
        obj->delRef();
        
    obj = aptr.obj;
    
    if (obj)
       obj->newRef();    
       
    return *this;
}	    

template<class T>   
inline T& ManagedPointer<T>::operator *() const
{
    return *obj;
}	    

template<class T>
inline T* ManagedPointer<T>::operator ->() const
{
    return obj;
}

template<class T>
inline ManagedPointer<T>::operator T*() const
{
    return obj;
}

template<class T>
inline bool ManagedPointer<T>::operator ==(const ManagedPointer<T> &aptr) const
{
    return obj == aptr.obj;
}

template<class T>
inline bool ManagedPointer<T>::operator !=(const ManagedPointer<T> &aptr) const
{
    return obj != aptr.obj;
}

template<class T>
inline long ManagedPointer<T>::refCount() const
{
    return obj->refCount();
}

#endif // MANAGEDPOINTER_H

	 
