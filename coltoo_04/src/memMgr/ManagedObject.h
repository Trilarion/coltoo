/*
 *  Copyright (C) 2003  The colonization too Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
/*
    Basic memory management with reference counting.
    Ignoring issues of stack vs heap allocation for now.
*/
        
#ifndef MANAGEDOBJECT_H
#define MANAGEDOBJECT_H

#include <list>
using std::list;
#include <iostream>

class ManagedObject
{
    public:
        void newRef(); //Add reference
        void delRef(); //Remove reference
        
        static void cleanObsolete(); //Clear objects no longer in use
        static void cleanupAll();    //Clear all objects (end of program)

        //For debugging
               long          getRefCount() const;
        static long          usedObjCount();
        static long          obsoleteObjCount();
        static unsigned long usedObjSize();
        static unsigned long obsoleteObjSize();
        static void          objStats();
                  
    protected:
        //Protected constructor and destructor to avoid direct instantiation
        ManagedObject();
		virtual ~ManagedObject();
        
    private:
        static list<ManagedObject*> usedObj;     //Track objects in use
        static list<ManagedObject*> obsoleteObj; //Objects no longer used

        long refCount;       //Number of times object is referenced to

		virtual unsigned long size()=0; //For tracking memory usage       
};

#define AUTO_SIZE unsigned long size(){return sizeof(*this);}

#endif //MANAGEDOBJECT_H
