Introduction
************
This is only a first draft for a conceptual overview of the core classes for
Coltoo.  This means no methods and attributes are defined yet and the actual
implementation may be slightly different (helper classes, more relations,
...).  Some parts are more detailed than others and parts suchs as the
networking classes are included as an example how we can extend this model in
the future.  We do not need to implement this immediately.

GameEngine and friends
**********************
The general idea is to expose an interface for feeding input to the
GameEngine.  This engine is part of the game's kernel and handles the "main
game loop".  Input can come from various sources such as a mouse or a
keyboard (HID: human input devices).  The AI doesn't use this and has to
create input events.  GameEngine takes this input, processes it and returns
an output event.  For humans this is sent to the screen.  The AI receives a
message with the results.
A clean way to separate all this is to let classes subscribe to other classes
for receiving events using an interface.  So the output classes can subscribe to
the GameEngine and do whatever they want to do when the engine notifies them of
an action.  Similarly the GameEngine can subscribre to the input source to
listen for commands.
To reduce clutter in GameEngine the grunt work of taking input and sending
output can be delegated to other classes (InputCollector and OutputDispatcher).

GameEngine is still responsible for the higher level processing.
Processing an input event includes:
	- Validating the input (syntax check): does the input make sense, is it
		correctly formatted.  It doesn't make sense to issue 'buy' commands when
		we're on the main map screen.  Invalid input could be either silently
		ignored or translated to an error message for the output classes.
	- Validating the command: if the command is syntactically correct we have to
		check if the command can be executed.  Buying something when there is no
		money is not allowed.  The validation will often be delegated to another
		subsystem, e.g. the CombatEngine can check if a unit can attack.
		If this validation fails an error message can be returned (OutputEvent).
	- Updating the universe: if the command is valid the universe can be updated
		after processing.  E.g. a units is killed or the terrain was modified.
	- Producing output.  E.g. when moving a unit, the screen is updated.

The Universe
************
GameUniverse is a clean version of the current GlobalData class.  It contains
common and high-level information for the current game such as the world map,
the current game year and the list of players.  The universe is not static!
Players can be killed, seasons pass, terrain changes, ...  This means the
universe needs to expose methods for modifying data.  Calls to the
GameUniverse should as much as possible be relayed through the GameEngine.  We
should consider only giving GameEngine "write access".

Actors
******
Of course, the game doesn't work without players.  The general interface for
players is defined in Actor.  An Actor can be either human controlled or
computer controlled.  In the case of an AI we can implement several strategies
(so far only the AiStrategy base class is included as a placeholder).  Humans
of course do some thinking of their own :)

Note that Actors are not limited to the colonizing nations.  Natives and the
REF are Actors too.  

An Actor has no properties that relate to a certain nation.  Those properties
are contained in a separate class.  NationProperties holds things such as:
name, colour, aggression, ...  These variables affect what an Actor can and
will do.
In case of a HumanPlayer some attributes may not be used (e.g. aggression, a
human player can be a peaceful Spaniard :)).

Settlements
***********
Settlements come in different forms.  The most obvious one is a Colony.
However, a native village and the Europeans ports are some kind of settlement
too.  Each settlement has a number of tiles to work on.  These are regular map
tiles, but they can hold a link to a worker from a certain colony.

Settlements have buildings, e.g. a lumber mill in a colony or the
university in the European port.  The docks and stockade (outside the colony)
are some kind of building too.  A building needs to know both it's owner and
the settlement it belongs to.  In the case of a mission it belongs to a
colonizing nation but it is part of a native settlement.

Each settlement also has a market.  This is where the amount and price of
goods is stored or determined.  The market is necessary to enable trade
between settlements (trade with natives or with the European port).

When implementing this we may choose to add a Painter class for each kind of
colony.  This way a colony can draw itself if the GraphicsEngine requests so.
I'm not sure yet if this is a good solution.

Units
*****
A Unit belongs to a player and has a position.  In theory this is enough
information to make it possible to draw the units on the map.  In the actual
implementation we will probably have to store lists of units in tiles and
settlement buildings too.
